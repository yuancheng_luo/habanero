#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef __APPLE__
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
#elif __unix__
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
#else
#include <io.h>
#endif

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "fftw3.h"
#include "vsappbbx.h"
#include "vsengine.h"

#include "hrtf-cp048-r1.h"

#define	_BLEND_LEN	192
#define	_ONEPARSEC	3.08567758e16

#define	_E_TT		1.0e-2
#define	_E_WW		2.5e-4
#define	_E_XX		1.38777999999999990e-17
#define	_E_YY		2.77556000000000000e-14
#define	_E_ZZ		1.00000000000000000e-07
#define	_M_E		2.71828182845904523540
#define	_M_PI		3.14159265358979323846

#define	_VSAPPBBX_VERSN_MAJOR		1
#define	_VSAPPBBX_VERSN_MINOR		2
#define	_VSAPPBBX_RELEASEDATE		20160718
#define	_VSAPPBBX_MAGICNUMBER		20160228

#define	_DEFAULT_HEAD_RADIUS		0.10
#define	_DEFAULT_TRSO_RADIUS		0.23
#define	_DEFAULT_NECK_HEIGHT		0.05

#define	_XOVER_FREQUENCY		380

#define	_MIN_ALLOWED_PFREQUENCY		3200
#define	_MAX_ALLOWED_PFREQUENCY		96000

#define	_HF_GAIN_ORDER_0		1.414213562373095 // sqrt(2)
#define	_HF_GAIN_ORDER_X		0.816496580927726 // sqrt(2/3)

//	n is the degree and is from zero to N
//	m is the order and is between -n and n

//	ACHTUNG:
//	sequence of ambisonics channels in B-format is as follows
//	(0 0)
//	(1 1) (1 -1) (1 0) <-- NOTE THIS LINE IS NOT IN THE USUAL ORDER
//	(2 0) (2 1) (2 -1) (2 2) (2 -2)
//	(3 0) (3 1) (3 -1) (3 2) (3 -2) (3 3) (3 -3)

struct 	_vsappbbx_istate { struct _hrtf_data *_h0; struct _hrtf_hash *_f0; struct _source_information **_s0; struct _source_work_area **_w0; struct _source_information *_t0; struct _source_work_area *_u0; int _mcok, _dsfq, _nsrc, _anch, _xord, _wght, _uoom, _sflt, _xhdl; float **_pw; double *_stht, *_spsi; float *_ixdx, *_tdta, *_oxtl, *_oxtr, *_sttl, *_stth; double _la0, _la1, _la2, _lb0, _lb1, _lb2, _ha0, _ha1, _ha2, _hb0, _hb1, _hb2; float *_inm2, *_inm1, *_lnm2, *_lnm1, *_hnm2, *_hnm1; };

double	_fact(int _x) { double _y = 1; int k; for (k = 1; k <= _x; k++) _y *= k; return(_y); }

int	_m1_pow(int _N) { return(_N % 2 ? -1 : 1); }

int	_sph_degree(int l, int _order_type) { return((int)sqrt(l)); } /* does not depend on order type, actually, except perhaps for SID order, which is currently not supported */

int	_sph_xorder(int l, int _order_type) {
	int _fuma_otbl[16] = { 0, 1, -1, 0, 0, 1, -1, 2, -2, 0, 1, -1, 2, -2, 3, -3 };

	if (_order_type == _CHANNEL_XORDER_ACN) return(l - (int)sqrt(l) * ((int)sqrt(l) + 1));

	if (_order_type == _CHANNEL_XORDER_FUMA) if (0 <= l && l < 16) return(_fuma_otbl[l]);

	puts("*** brain damage (sph section) ***"); exit(1);
}

double	_fpsign(double _a, double _b) { return(_b >= 0.0 ? fabs(_a) : -fabs(_a)); }

double	_pythag(double _a, double _b) {
	double _absa, _absb;

	_absa = fabs(_a); _absb = fabs(_b);

	if (_absa > _absb)
		return (_absa * sqrt(1.0 + (_absb / _absa) * (_absb / _absa)));
	else
		if (_absb)
			return(_absb * sqrt(1.0 + (_absa / _absb) * (_absa / _absb)));
		else
			return(0.00);
}

double	_plgndr(int n, int m, double x) {
	double fact, pll, pmm = 1.0, pmmp1, somx2; int i, ll;

	if (m >= 0 && m <= n && fabs(x) <= 1.0); else { puts("*** WARNING: degree / order invalid on mode strength computation"); return(0.00); }

	if (m > 0) { somx2 = sqrt((1.0 - x)*(1.0 + x)); fact = 1.0; for (i = 1; i <= m; i++) { pmm *= -fact*somx2; fact += 2.0; } }

	if (n == m) return(pmm);

	pmmp1 = x*(2 * m + 1)*pmm;

	if (n == (m + 1)) return(pmmp1);

	for (ll = m + 2; ll <= n; ll++) { pll = (x*(2 * ll - 1)*pmmp1 - (ll + m - 1)*pmm) / (ll - m); pmm = pmmp1; pmmp1 = pll; }

	return(pll);
}

int	_svdcmp(double **a, int m, int n, double *w, double **v) {
	int flag, i, its, j, jj, k, l, nm; double anorm = 0.0, c, f, g = 0.0, h, s, scale = 0.0, x, y, z; double *_rv1 = NULL;

	_rv1 = (double *)malloc((n + 1) * sizeof(double)); if (_rv1); else return(-1);

	for (i = 1; i <= n; i++) {
		l = i + 1;
		_rv1[i] = scale*g;
		g = s = scale = 0.0;
		if (i <= m) {
			for (k = i; k <= m; k++) scale += fabs(a[k][i]);
			if (scale) {
				for (k = i; k <= m; k++) {
					a[k][i] /= scale;
					s += a[k][i] * a[k][i];
				}
				f = a[i][i];
				g = -_fpsign(sqrt(s), f);
				h = f*g - s;
				a[i][i] = f - g;
				for (j = l; j <= n; j++) {
					for (s = 0.0, k = i; k <= m; k++) s += a[k][i] * a[k][j];
					f = s / h;
					for (k = i; k <= m; k++) a[k][j] += f*a[k][i];
				}
				for (k = i; k <= m; k++) a[k][i] *= scale;
			}
		}
		w[i] = scale *g;
		g = s = scale = 0.0;
		if (i <= m && i != n) {
			for (k = l; k <= n; k++) scale += fabs(a[i][k]);
			if (scale) {
				for (k = l; k <= n; k++) {
					a[i][k] /= scale;
					s += a[i][k] * a[i][k];
				}
				f = a[i][l];
				g = -_fpsign(sqrt(s), f);
				h = f*g - s;
				a[i][l] = f - g;
				for (k = l; k <= n; k++) _rv1[k] = a[i][k] / h;
				for (j = l; j <= m; j++) {
					for (s = 0.0, k = l; k <= n; k++) s += a[j][k] * a[i][k];
					for (k = l; k <= n; k++) a[j][k] += s*_rv1[k];
				}
				for (k = l; k <= n; k++) a[i][k] *= scale;
			}
		}
		if (anorm < fabs(w[i]) + fabs(_rv1[i])) anorm = fabs(w[i]) + fabs(_rv1[i]);
	}
	for (i = n; i >= 1; i--) {
		if (i < n) {
			if (g) {
				for (j = l; j <= n; j++)
					v[j][i] = (a[i][j] / a[i][l]) / g;
				for (j = l; j <= n; j++) {
					for (s = 0.0, k = l; k <= n; k++) s += a[i][k] * v[k][j];
					for (k = l; k <= n; k++) v[k][j] += s*v[k][i];
				}
			}
			for (j = l; j <= n; j++) v[i][j] = v[j][i] = 0.0;
		}
		v[i][i] = 1.0;
		g = _rv1[i];
		l = i;
	}
	for (i = (m < n ? m : n); i >= 1; i--) {
		l = i + 1;
		g = w[i];
		for (j = l; j <= n; j++) a[i][j] = 0.0;
		if (g) {
			g = 1.0 / g;
			for (j = l; j <= n; j++) {
				for (s = 0.0, k = l; k <= m; k++) s += a[k][i] * a[k][j];
				f = (s / a[i][i])*g;
				for (k = i; k <= m; k++) a[k][j] += f*a[k][i];
			}
			for (j = i; j <= m; j++) a[j][i] *= g;
		}
		else for (j = i; j <= m; j++) a[j][i] = 0.0;
		++a[i][i];
	}
	for (k = n; k >= 1; k--) {
		for (its = 1; its <= 30; its++) {
			flag = 1;
			for (l = k; l >= 1; l--) {
				nm = l - 1;
				if ((double)(fabs(_rv1[l]) + anorm) == anorm) {
					flag = 0;
					break;
				}
				if ((double)(fabs(w[nm]) + anorm) == anorm) break;
			}
			if (flag) {
				c = 0.0;
				s = 1.0;
				for (i = l; i <= k; i++) {
					f = s*_rv1[i];
					_rv1[i] = c*_rv1[i];
					if ((double)(fabs(f) + anorm) == anorm) break;
					g = w[i];
					h = _pythag(f, g);
					w[i] = h;
					h = 1.0 / h;
					c = g*h;
					s = -f*h;
					for (j = 1; j <= m; j++) {
						y = a[j][nm];
						z = a[j][i];
						a[j][nm] = y*c + z*s;
						a[j][i] = z*c - y*s;
					}
				}
			}
			z = w[k];
			if (l == k) {
				if (z < 0.0) {
					w[k] = -z;
					for (j = 1; j <= n; j++) v[j][k] = -v[j][k];
				}
				break;
			}
			if (its == 30) return(-2);
			x = w[l];
			nm = k - 1;
			y = w[nm];
			g = _rv1[nm];
			h = _rv1[k];
			f = ((y - z)*(y + z) + (g - h)*(g + h)) / (2.0*h*y);
			g = _pythag(f, 1.0);
			f = ((x - z)*(x + z) + h*((y / (f + _fpsign(g, f))) - h)) / x;
			c = s = 1.0;
			for (j = l; j <= nm; j++) {
				i = j + 1;
				g = _rv1[i];
				y = w[i];
				h = s*g;
				g = c*g;
				z = _pythag(f, h);
				_rv1[j] = z;
				c = f / z;
				s = h / z;
				f = x*c + g*s;
				g = g*c - x*s;
				h = y*s;
				y *= c;
				for (jj = 1; jj <= n; jj++) {
					x = v[jj][j];
					z = v[jj][i];
					v[jj][j] = x*c + z*s;
					v[jj][i] = z*c - x*s;
				}
				z = _pythag(f, h);
				w[j] = z;
				if (z) {
					z = 1.0 / z;
					c = f*z;
					s = h*z;
				}
				f = c*g + s*y;
				x = c*y - s*g;
				for (jj = 1; jj <= m; jj++) {
					y = a[jj][j];
					z = a[jj][i];
					a[jj][j] = y*c + z*s;
					a[jj][i] = z*c - y*s;
				}
			}
			_rv1[l] = 0.0;
			_rv1[k] = f;
			w[k] = x;
		}
	}

	free(_rv1); return(0);
}

double	_fuma_weight(int n, int m) {

	if (n == 0 && m == 0) return(1.0 / sqrt(2.0));

	if (n == 1 && m == -1) return(1.0);
	if (n == 1 && m == 0) return(1.0);
	if (n == 1 && m == 1) return(1.0);

	if (n == 2 && m == -2) return(2.0 / sqrt(3.0));
	if (n == 2 && m == -1) return(2.0 / sqrt(3.0));
	if (n == 2 && m == 0) return(1.0);
	if (n == 2 && m == 1) return(2.0 / sqrt(3.0));
	if (n == 2 && m == 2) return(2.0 / sqrt(3.0));

	if (n == 3 && m == -3) return(sqrt(8.0 / 5.0));
	if (n == 3 && m == -2) return(3.0 / sqrt(5.0));
	if (n == 3 && m == -1) return(sqrt(45.0 / 32.0));
	if (n == 3 && m == 0) return(1.0);
	if (n == 3 && m == 1) return(sqrt(45.0 / 32.0));
	if (n == 3 && m == 2) return(3.0 / sqrt(5.0));
	if (n == 3 && m == 3) return(sqrt(8.0 / 5.0));

	puts("*** brain damage (fmw section) ***"); exit(1);
}

double	_sph_value_ambisonic(int n, int m, double _tht, double _psi, int _weighting_type) {
	int mm; double _qv, _wf = 1.0, _nft, _nfb;

	//	ACHTUNG -- REMEMBER TO CONVERT THE ANGLES WHEN CALLING THIS FUNCTION
	//	it expects ambisonics-format angles at the input
	//	elevation _tht should be is from -pi/2 to pi/2
	//	zero elevation is horizontal, pi/2 is up, -pi/2 is down
	//	azimuth _phi is increasing counterclockwise; zero azimuth is in front

	if (0 <= n); else { puts("*** ERROR: out of range n input to _sph_value"); exit(1); }

	if (-n <= m && m <= n); else { puts("*** ERROR: out of range m input to _sph_value"); exit(1); }

	if (m < 0) mm = -m; else mm = m;

	_nft = _fact(n - mm); _nfb = _fact(n + mm);

	if (_weighting_type == _CHANNEL_WEIGHT_N3D) _wf = sqrt(2.0 * (double)n + 1.0);
	if (_weighting_type == _CHANNEL_WEIGHT_SN3D) _wf = 1.0;
	if (_weighting_type == _CHANNEL_WEIGHT_FUMA) _wf = _fuma_weight(n, m);

	_qv = _m1_pow(mm) * _wf * sqrt((2 - (mm ? 0 : 1)) * (double)_nft / (double)_nfb) * _plgndr(n, mm, sin(_tht));

	if (m < 0) return(_qv * sin(mm * _psi)); else return(_qv * cos(mm * _psi));
}

int	_compute_weight_matrix(struct _vsappbbx_istate *_i0) {
	double **_a, *_w, **_v, **_i; int k, l, m;

	//	_a has _anch rows and _nsrc columns
	//	_w is a vector with _nsrc elements
	//	_v is a square matrix of dimension _nsrc

	_a = (double **)malloc((_i0->_anch + 1) * sizeof(double *)); if (_a); else return(-1); _a[0] = NULL;

	for (k = 1; k < _i0->_anch + 1; k++) _a[k] = (double *)malloc((_i0->_nsrc + 1) * sizeof(double));

	for (k = 1; k < _i0->_anch + 1; k++) if (_a[k]); else return(-1);


	_v = (double **)malloc((_i0->_nsrc + 1) * sizeof(double *)); if (_v); else return(-1); _v[0] = NULL;

	for (l = 1; l < _i0->_nsrc + 1; l++) _v[l] = (double *)malloc((_i0->_nsrc + 1) * sizeof(double));

	for (l = 1; l < _i0->_nsrc + 1; l++) if (_v[l]); else return(-1);


	for (k = 0; k < _i0->_anch; k++) for (l = 0; l < _i0->_nsrc; l++) _a[k + 1][l + 1] = _sph_value_ambisonic(_sph_degree(k, _i0->_xord), _sph_xorder(k, _i0->_xord), _M_PI / 2 - _i0->_stht[l], -_i0->_spsi[l], _i0->_wght);


	_w = (double *)malloc((_i0->_nsrc + 1) * sizeof(double)); if (_w); else return(-1);


	if (_svdcmp(_a, _i0->_anch, _i0->_nsrc, _w, _v)) return(-1);


	for (l = 0; l < _i0->_nsrc; l++) if (_w[l + 1] < _E_YY) _w[l + 1] = 0; else _w[l + 1] = 1.0 / _w[l + 1];


	_i = (double **)malloc((_i0->_nsrc + 1) * sizeof(double *)); if (_i); else return(-1); _i[0] = NULL;

	for (l = 1; l < _i0->_nsrc + 1; l++) _i[l] = (double *)malloc((_i0->_anch + 1) * sizeof(double));

	for (l = 1; l < _i0->_nsrc + 1; l++) if (_i[l]); else return(-1);

	for (l = 1; l < _i0->_nsrc + 1; l++) memset(_i[l], 0, (_i0->_anch + 1) * sizeof(double));

	for (l = 0; l < _i0->_nsrc; l++) for (k = 0; k < _i0->_anch; k++) for (m = 0; m < _i0->_nsrc; m++) _i[l + 1][k + 1] += _w[m + 1] * _v[l + 1][m + 1] * _a[k + 1][m + 1];

	//	now _i is the playback matrix, size _i0->_nsrc by _i0->_anch, still in NR format

	_i0->_pw = (float **)malloc(_i0->_nsrc * sizeof(float *)); if (_i0->_pw); else return(-1);

	for (l = 0; l < _i0->_nsrc; l++) _i0->_pw[l] = (float *)malloc(_i0->_anch * sizeof(float));

	for (l = 0; l < _i0->_nsrc; l++) if (_i0->_pw[l]); else return(-1);

	for (l = 0; l < _i0->_nsrc; l++) for (k = 0; k < _i0->_anch; k++) _i0->_pw[l][k] = _i[l + 1][k + 1];


	for (l = 0; l < _i0->_nsrc; l++, printf("\n")) for (k = 0; k < _i0->_anch; k++) printf(" %lf", _i0->_pw[l][k]);


	//	free up everything (yes, EVERYTHING)

	for (l = 1; l < _i0->_nsrc + 1; l++) free(_i[l]); free(_i);

	for (l = 1; l < _i0->_nsrc + 1; l++) free(_v[l]); free(_v);

	for (k = 1; k < _i0->_anch + 1; k++) free(_a[k]); free(_a);

	free(_w); return(0);
}

int	_process_xdta(struct _vsappbbx_istate *_i0, void *_xdta) {
	int _xversion, _xchorder, _xcweight, _xpccount, _xgpfield, _xhrtfxln;

	if (_xdta); else return(0); _xversion = *(short *)((char *)_xdta + 0x00);

	if (_xversion == 1) {

		_xchorder = *(short *)((char *)_xdta + 0x02);
		_xcweight = *(short *)((char *)_xdta + 0x04);
		_xpccount = *(short *)((char *)_xdta + 0x06);
		_xhrtfxln = *(int   *)((char *)_xdta + 0x08);
		_xgpfield = *(int   *)((char *)_xdta + 0x0C);

		if (_xgpfield & _GPFIELD_ENABLE_TAIL) _i0->_uoom = 1;
		if (_xgpfield & _GPFIELD_ENABLE_SFLT) _i0->_sflt = 1;

		if (_xchorder == _CHANNEL_XORDER_FUMA || _xchorder == _CHANNEL_XORDER_ACN || _xchorder == _CHANNEL_XORDER_SID); else return(_VSAPPBBX_UNK_CHANNEL_XORDER);
		if (_xcweight == _CHANNEL_WEIGHT_FUMA || _xcweight == _CHANNEL_WEIGHT_SN3D || _xcweight == _CHANNEL_WEIGHT_N3D); else return(_VSAPPBBX_UNK_CHANNEL_WEIGHT);

		if (_xchorder == _CHANNEL_XORDER_FUMA) if (_xcweight == _CHANNEL_WEIGHT_FUMA); else return(_VSAPPBBX_UNSUPPORTED_SSETUP);
		if (_xcweight == _CHANNEL_WEIGHT_FUMA) if (_xchorder == _CHANNEL_XORDER_FUMA); else return(_VSAPPBBX_UNSUPPORTED_SSETUP);

		if (_xchorder == _CHANNEL_XORDER_FUMA) if (_i0->_anch <= 16); else return(_VSAPPBBX_UNSUPPORTED_SSETUP);

		if (_xchorder == _CHANNEL_XORDER_SID) return(_VSAPPBBX_UNSUPPORTED_SSETUP); /* don't know what is SID order */

		if (0 <= _xpccount && _xpccount <= 1024); else return(_VSAPPBBX_UNSUPPORTED_NVLS);

		_i0->_xhdl = _xhrtfxln; _i0->_xord = _xchorder; _i0->_wght = _xcweight; return(0);
	}

	return(_VSAPPBBX_UNK_XDTA_VERSION);
}

int	_establish_speaker_arrangement(struct _vsappbbx_istate *_i0, void *_xdta) {
	int l = 0; int _xversion = 0, _xpccount = 0; double *_ztht, *_zpsi;

	if (_xdta) _xversion = *(short *)((char *)_xdta + 0x00);

	if (_xversion == 1) _xpccount = *(short *)((char *)_xdta + 0x06);

	if (_xversion == 1) if (_xpccount) {
		_i0->_nsrc = _xpccount;

		_i0->_stht = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_stht); else return(_VSAPPBBX_MALLOC_FAILURE);
		_i0->_spsi = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_spsi); else return(_VSAPPBBX_MALLOC_FAILURE);

		_ztht = *(double **)((char *)_xdta + 0x10 + 0 * sizeof(double *)); for (l = 0; l < _i0->_nsrc; l++) _i0->_stht[l] = _ztht[l];
		_zpsi = *(double **)((char *)_xdta + 0x10 + 1 * sizeof(double *)); for (l = 0; l < _i0->_nsrc; l++) _i0->_spsi[l] = _zpsi[l];

		return(0);
	}

	if (_i0->_anch == 4) {
		_i0->_nsrc = 8; /* first order ambisonics, speakers = vertices of a cube */

		_i0->_stht = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_stht); else return(_VSAPPBBX_MALLOC_FAILURE);
		_i0->_spsi = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_spsi); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = -0.785398; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = -0.785398; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = -2.356194; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = -2.356194; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = 2.356194; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = 2.356194; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = 0.785398; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = 0.785398; l++; return(0);
	}

	if (_i0->_anch == 9) {
		_i0->_nsrc = 20; /* second order ambisonics, speakers = vertices of a regular dodecahedron */

		_i0->_stht = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_stht); else return(_VSAPPBBX_MALLOC_FAILURE);
		_i0->_spsi = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_spsi); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = 2.356194; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = 0.785398; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = 2.356194; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = 0.785398; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = -2.356194; l++;
		_i0->_stht[l] = 2.186276; _i0->_spsi[l] = -0.785398; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = -2.356194; l++;
		_i0->_stht[l] = 0.955316; _i0->_spsi[l] = -0.785398; l++;
		_i0->_stht[l] = 2.776728; _i0->_spsi[l] = 3.141593; l++;
		_i0->_stht[l] = 0.364864; _i0->_spsi[l] = 3.141593; l++;
		_i0->_stht[l] = 2.776728; _i0->_spsi[l] = 0.000000; l++;
		_i0->_stht[l] = 0.364864; _i0->_spsi[l] = 0.000000; l++;
		_i0->_stht[l] = 1.570796; _i0->_spsi[l] = -2.776729; l++;
		_i0->_stht[l] = 1.570796; _i0->_spsi[l] = 2.776729; l++;
		_i0->_stht[l] = 1.570796; _i0->_spsi[l] = -0.364864; l++;
		_i0->_stht[l] = 1.570796; _i0->_spsi[l] = 0.364864; l++;
		_i0->_stht[l] = 1.205932; _i0->_spsi[l] = -1.570796; l++;
		_i0->_stht[l] = 1.205932; _i0->_spsi[l] = 1.570796; l++;
		_i0->_stht[l] = 1.935660; _i0->_spsi[l] = -1.570796; l++;
		_i0->_stht[l] = 1.935660; _i0->_spsi[l] = 1.570796; l++; return(0);
	}

	if (_i0->_anch == 16) {
		_i0->_nsrc = 32; /* third order ambisonics, speakers = faces of a truncated icosahedron */

		_i0->_stht = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_stht); else return(_VSAPPBBX_MALLOC_FAILURE);
		_i0->_spsi = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_spsi); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_stht[l] = 3.1415926; _i0->_spsi[l] = 0.0000000; l++;
		_i0->_stht[l] = 2.0344443; _i0->_spsi[l] = -2.1991149; l++;
		_i0->_stht[l] = 2.0344443; _i0->_spsi[l] = 2.8274331; l++;
		_i0->_stht[l] = 2.0344443; _i0->_spsi[l] = -0.9424778; l++;
		_i0->_stht[l] = 2.0344443; _i0->_spsi[l] = 1.5707963; l++;
		_i0->_stht[l] = 2.0344443; _i0->_spsi[l] = 0.3141595; l++;
		_i0->_stht[l] = 1.1071483; _i0->_spsi[l] = -1.5707964; l++;
		_i0->_stht[l] = 1.1071483; _i0->_spsi[l] = -2.8274331; l++;
		_i0->_stht[l] = 1.1071483; _i0->_spsi[l] = 2.1991149; l++;
		_i0->_stht[l] = 1.1071483; _i0->_spsi[l] = -0.3141595; l++;
		_i0->_stht[l] = 1.1071483; _i0->_spsi[l] = 0.9424778; l++;
		_i0->_stht[l] = 0.0000000; _i0->_spsi[l] = 0.0000000; l++;
		_i0->_stht[l] = 2.4892341; _i0->_spsi[l] = -2.8274331; l++;
		_i0->_stht[l] = 2.4892341; _i0->_spsi[l] = -1.5707964; l++;
		_i0->_stht[l] = 2.4892341; _i0->_spsi[l] = 2.1991149; l++;
		_i0->_stht[l] = 2.4892341; _i0->_spsi[l] = -0.3141595; l++;
		_i0->_stht[l] = 2.4892341; _i0->_spsi[l] = 0.9424778; l++;
		_i0->_stht[l] = 1.7595065; _i0->_spsi[l] = -1.5707964; l++;
		_i0->_stht[l] = 1.7595065; _i0->_spsi[l] = -2.8274331; l++;
		_i0->_stht[l] = 1.7595065; _i0->_spsi[l] = 2.1991149; l++;
		_i0->_stht[l] = 1.3820861; _i0->_spsi[l] = -2.1991149; l++;
		_i0->_stht[l] = 1.7595065; _i0->_spsi[l] = -0.3141595; l++;
		_i0->_stht[l] = 1.3820861; _i0->_spsi[l] = 2.8274331; l++;
		_i0->_stht[l] = 1.7595065; _i0->_spsi[l] = 0.9424778; l++;
		_i0->_stht[l] = 1.3820861; _i0->_spsi[l] = -0.9424778; l++;
		_i0->_stht[l] = 1.3820861; _i0->_spsi[l] = 1.5707963; l++;
		_i0->_stht[l] = 0.6523585; _i0->_spsi[l] = -2.1991149; l++;
		_i0->_stht[l] = 1.3820861; _i0->_spsi[l] = 0.3141595; l++;
		_i0->_stht[l] = 0.6523585; _i0->_spsi[l] = 2.8274331; l++;
		_i0->_stht[l] = 0.6523585; _i0->_spsi[l] = -0.9424778; l++;
		_i0->_stht[l] = 0.6523585; _i0->_spsi[l] = 1.5707963; l++;
		_i0->_stht[l] = 0.6523585; _i0->_spsi[l] = 0.3141595; l++; return(0);
	}

	if (_i0->_anch == 25 || _i0->_anch == 36 || _i0->_anch == 49 || _i0->_anch == 64) {
		_i0->_nsrc = 64; /* for now, anything higher than 3rd order is played via the audio camera grid */

		_i0->_stht = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_stht); else return(_VSAPPBBX_MALLOC_FAILURE);
		_i0->_spsi = (double *)malloc(_i0->_nsrc * sizeof(double)); if (_i0->_spsi); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_stht[l] = 1.7590450; _i0->_spsi[l] = 2.3917300; l++;
		_i0->_stht[l] = 1.3370430; _i0->_spsi[l] = 2.1966630; l++;
		_i0->_stht[l] = 1.2526390; _i0->_spsi[l] = 1.7443250; l++;
		_i0->_stht[l] = 1.1024650; _i0->_spsi[l] = 1.2902280; l++;
		_i0->_stht[l] = 1.4561830; _i0->_spsi[l] = 0.9244470; l++;
		_i0->_stht[l] = 1.7017460; _i0->_spsi[l] = 0.5370090; l++;
		_i0->_stht[l] = 2.1424740; _i0->_spsi[l] = 0.6424560; l++;
		_i0->_stht[l] = 2.5458630; _i0->_spsi[l] = 0.3576870; l++;
		_i0->_stht[l] = 2.9248520; _i0->_spsi[l] = 1.2374310; l++;
		_i0->_stht[l] = 2.5252180; _i0->_spsi[l] = 1.8923330; l++;
		_i0->_stht[l] = 2.1045100; _i0->_spsi[l] = 2.0721420; l++;
		_i0->_stht[l] = 1.6983430; _i0->_spsi[l] = 1.8669690; l++;
		_i0->_stht[l] = 1.5846760; _i0->_spsi[l] = 1.3953540; l++;
		_i0->_stht[l] = 1.9041460; _i0->_spsi[l] = 1.0558960; l++;
		_i0->_stht[l] = 2.4268250; _i0->_spsi[l] = 1.1470010; l++;
		_i0->_stht[l] = 2.0667240; _i0->_spsi[l] = 1.5311580; l++;
		_i0->_stht[l] = 2.4938620; _i0->_spsi[l] = -2.6799810; l++;
		_i0->_stht[l] = 2.1834540; _i0->_spsi[l] = -3.1117960; l++;
		_i0->_stht[l] = 1.8252490; _i0->_spsi[l] = 2.8538270; l++;
		_i0->_stht[l] = 1.3550170; _i0->_spsi[l] = 2.6720740; l++;
		_i0->_stht[l] = 0.9275080; _i0->_spsi[l] = 2.4143550; l++;
		_i0->_stht[l] = 0.8148490; _i0->_spsi[l] = 1.8339480; l++;
		_i0->_stht[l] = 0.6329750; _i0->_spsi[l] = 1.1408270; l++;
		_i0->_stht[l] = 1.0191790; _i0->_spsi[l] = 0.7760650; l++;
		_i0->_stht[l] = 1.2660500; _i0->_spsi[l] = 0.3803470; l++;
		_i0->_stht[l] = 1.6004990; _i0->_spsi[l] = 0.0628610; l++;
		_i0->_stht[l] = 2.0520380; _i0->_spsi[l] = 0.1244010; l++;
		_i0->_stht[l] = 2.3324510; _i0->_spsi[l] = -0.3359920; l++;
		_i0->_stht[l] = 2.7593860; _i0->_spsi[l] = -0.5597060; l++;
		_i0->_stht[l] = 2.8646440; _i0->_spsi[l] = -2.0286810; l++;
		_i0->_stht[l] = 2.6958540; _i0->_spsi[l] = 2.7817380; l++;
		_i0->_stht[l] = 2.2513340; _i0->_spsi[l] = 2.5950920; l++;
		_i0->_stht[l] = 1.8697700; _i0->_spsi[l] = -0.3699330; l++;
		_i0->_stht[l] = 2.1083230; _i0->_spsi[l] = -0.8358370; l++;
		_i0->_stht[l] = 2.4556070; _i0->_spsi[l] = -1.2312260; l++;
		_i0->_stht[l] = 2.3830520; _i0->_spsi[l] = -1.9685890; l++;
		_i0->_stht[l] = 2.0662450; _i0->_spsi[l] = -2.4694920; l++;
		_i0->_stht[l] = 1.7872400; _i0->_spsi[l] = -2.8590980; l++;
		_i0->_stht[l] = 1.4633850; _i0->_spsi[l] = 3.1320330; l++;
		_i0->_stht[l] = 1.0157370; _i0->_spsi[l] = 3.0450470; l++;
		_i0->_stht[l] = 0.5928930; _i0->_spsi[l] = 2.8492640; l++;
		_i0->_stht[l] = 0.3677770; _i0->_spsi[l] = 1.9799050; l++;
		_i0->_stht[l] = 0.2569820; _i0->_spsi[l] = 0.3177300; l++;
		_i0->_stht[l] = 0.7123310; _i0->_spsi[l] = 0.3275810; l++;
		_i0->_stht[l] = 1.0589930; _i0->_spsi[l] = -0.0660190; l++;
		_i0->_stht[l] = 1.4038390; _i0->_spsi[l] = -0.3491530; l++;
		_i0->_stht[l] = 1.5872940; _i0->_spsi[l] = -0.7672960; l++;
		_i0->_stht[l] = 1.7867720; _i0->_spsi[l] = -1.1678810; l++;
		_i0->_stht[l] = 2.0653510; _i0->_spsi[l] = -1.5370880; l++;
		_i0->_stht[l] = 1.8935130; _i0->_spsi[l] = -2.0087050; l++;
		_i0->_stht[l] = 1.5839420; _i0->_spsi[l] = -2.4402650; l++;
		_i0->_stht[l] = 1.2350940; _i0->_spsi[l] = -2.7424190; l++;
		_i0->_stht[l] = 0.7495100; _i0->_spsi[l] = -2.6936600; l++;
		_i0->_stht[l] = 0.2691120; _i0->_spsi[l] = -2.5635420; l++;
		_i0->_stht[l] = 0.4090060; _i0->_spsi[l] = -1.1469290; l++;
		_i0->_stht[l] = 0.6873440; _i0->_spsi[l] = -0.4325020; l++;
		_i0->_stht[l] = 1.0944930; _i0->_spsi[l] = -0.7079560; l++;
		_i0->_stht[l] = 1.3093680; _i0->_spsi[l] = -1.1786670; l++;
		_i0->_stht[l] = 1.5798630; _i0->_spsi[l] = -1.5842270; l++;
		_i0->_stht[l] = 1.4381460; _i0->_spsi[l] = -2.0118320; l++;
		_i0->_stht[l] = 1.0679960; _i0->_spsi[l] = -2.2641300; l++;
		_i0->_stht[l] = 0.6797900; _i0->_spsi[l] = -1.9276640; l++;
		_i0->_stht[l] = 0.8482350; _i0->_spsi[l] = -1.1920560; l++;
		_i0->_stht[l] = 1.1002330; _i0->_spsi[l] = -1.6660590; l++; return(0);
	}

	return(_VSAPPBBX_UNSUPPORTED_SSETUP); /* unknown input configuration */
}

void	_update_position(struct _vsappbbx_istate *_i0, double *_pdta) {
	int l;

	double _esx, _esy, _esz, _tx, _ty, _tz, _psi, _tht, _phi;
	double _a11, _a12, _a13, _a21, _a22, _a23, _a31, _a32, _a33;
	double _i11, _i12, _i13, _i21, _i22, _i23, _i31, _i32, _i33;

	for (l = 0; l < _i0->_nsrc; l++) {

		/* the speaker grid has regular angle conventions */
		/* azimuth zero is front, azimuth increases clockwise */
		/* elevation zero is top, elevation increases down */
		/* coordinate system is Polhemus one: X right Y back Z down */

		_esx = 1.0 * sin(_i0->_spsi[l]) * sin(_i0->_stht[l]);
		_esy = -1.0 * cos(_i0->_spsi[l]) * sin(_i0->_stht[l]);
		_esz = -1.0                      * cos(_i0->_stht[l]);

		_tx = 0.0;
		_ty = 0.0;
		_tz = 0.0;

		_psi = _pdta[0];
		_tht = _pdta[1];
		_phi = _pdta[2];

		/* the code below is your basic _invert_coordinates function */

		/* esx, esy, esz are coordinates of the point in tracker's coordinate system */
		/* these should be set to 0, 0, 0 to find out coordinates of a transmitter */
		/* in receiver coordinate frame */

		_a11 = cos(_psi) * cos(_tht);
		_a21 = sin(_psi) * cos(_tht);
		_a31 = -sin(_tht);

		_a12 = cos(_psi) * sin(_tht) * sin(_phi) - sin(_psi) * cos(_phi);
		_a22 = sin(_psi) * sin(_tht) * sin(_phi) + cos(_psi) * cos(_phi);
		_a32 = cos(_tht) * sin(_phi);

		_a13 = cos(_psi) * sin(_tht) * cos(_phi) + sin(_psi) * sin(_phi);
		_a23 = sin(_psi) * sin(_tht) * cos(_phi) - cos(_psi) * sin(_phi);
		_a33 = cos(_tht) * cos(_phi);

		_i11 = _a22 * _a33 - _a32 * _a23; _i12 = _a32 * _a13 - _a12 * _a33; _i13 = _a12 * _a23 - _a22 * _a13;
		_i21 = _a31 * _a23 - _a21 * _a33; _i22 = _a11 * _a33 - _a31 * _a13; _i23 = _a21 * _a13 - _a11 * _a23;
		_i31 = _a21 * _a32 - _a31 * _a22; _i32 = _a31 * _a12 - _a11 * _a32; _i33 = _a11 * _a22 - _a21 * _a12;

		_i0->_s0[l]->_source_ixyz[0] = _i11 * (_esx - _tx) + _i12 * (_esy - _ty) + _i13 * (_esz - _tz);
		_i0->_s0[l]->_source_ixyz[1] = _i21 * (_esx - _tx) + _i22 * (_esy - _ty) + _i23 * (_esz - _tz);
		_i0->_s0[l]->_source_ixyz[2] = _i31 * (_esx - _tx) + _i32 * (_esy - _ty) + _i33 * (_esz - _tz);
	}

	return;
}

void	_setup_bandsplit(struct _vsappbbx_istate *_i0, int _fc, int _fs) {
	double _k;

	_k = tan(_M_PI * (double)_fc / (double)_fs);

	_i0->_la0 = 1.0;
	_i0->_la1 = 2.0 * (_k * _k - 1.0) / (_k * _k + 2 * _k + 1.0);
	_i0->_la2 = (_k * _k - 2 * _k + 1.0) / (_k * _k + 2 * _k + 1.0);

	_i0->_ha0 = 1.0;
	_i0->_ha1 = 2.0 * (_k * _k - 1.0) / (_k * _k + 2 * _k + 1.0);
	_i0->_ha2 = (_k * _k - 2 * _k + 1.0) / (_k * _k + 2 * _k + 1.0);

	_i0->_lb0 = _k * _k / (_k * _k + 2 * _k + 1.0);
	_i0->_lb1 = 2.0 * _k * _k / (_k * _k + 2 * _k + 1.0);
	_i0->_lb2 = _k * _k / (_k * _k + 2 * _k + 1.0);

	_i0->_hb0 = 1.0 / (_k * _k + 2 * _k + 1.0);
	_i0->_hb1 = -2.0 / (_k * _k + 2 * _k + 1.0);
	_i0->_hb2 = 1.0 / (_k * _k + 2 * _k + 1.0);

	return;
}

void	_split_filter_combine(struct _vsappbbx_istate *_i0, float **_data) {
	int _bsze; int k; float _gain; int p;

	_bsze = _i0->_s0[0]->_data_chunk_size;

	for (k = 0; k < _i0->_anch; k++) {

		// apply LP filter; sttl is the output of the filtering operation

		_i0->_sttl[0] = _i0->_lb0 * _data[k][0] + _i0->_lb1 * _i0->_inm1[k] + _i0->_lb2 * _i0->_inm2[k] - _i0->_la1 * _i0->_lnm1[k] - _i0->_la2 * _i0->_lnm2[k];

		_i0->_sttl[1] = _i0->_lb0 * _data[k][1] + _i0->_lb1 *   _data[k][0] + _i0->_lb2 * _i0->_inm1[k] - _i0->_la1 * _i0->_sttl[0] - _i0->_la2 * _i0->_lnm1[k];

		for (p = 2; p < _bsze; p++) _i0->_sttl[p] = _i0->_lb0 * _data[k][p] + _i0->_lb1 * _data[k][p - 1] + _i0->_lb2 * _data[k][p - 2] - _i0->_la1 * _i0->_sttl[p - 1] - _i0->_la2 * _i0->_sttl[p - 2];


		// apply HP filter; stth is the output of the filtering operation

		_i0->_stth[0] = _i0->_hb0 * _data[k][0] + _i0->_hb1 * _i0->_inm1[k] + _i0->_hb2 * _i0->_inm2[k] - _i0->_ha1 * _i0->_hnm1[k] - _i0->_ha2 * _i0->_hnm2[k];

		_i0->_stth[1] = _i0->_hb0 * _data[k][1] + _i0->_hb1 *   _data[k][0] + _i0->_hb2 * _i0->_inm1[k] - _i0->_ha1 * _i0->_stth[0] - _i0->_ha2 * _i0->_hnm1[k];

		for (p = 2; p < _bsze; p++) _i0->_stth[p] = _i0->_hb0 * _data[k][p] + _i0->_hb1 * _data[k][p - 1] + _i0->_hb2 * _data[k][p - 2] - _i0->_ha1 * _i0->_stth[p - 1] - _i0->_ha2 * _i0->_stth[p - 2];


		// store processed data for the next call

		_i0->_inm1[k] = _data[k][_bsze - 1]; _i0->_inm2[k] = _data[k][_bsze - 2];
		_i0->_lnm1[k] = _i0->_sttl[_bsze - 1]; _i0->_lnm2[k] = _i0->_sttl[_bsze - 2];
		_i0->_hnm1[k] = _i0->_stth[_bsze - 1]; _i0->_hnm2[k] = _i0->_stth[_bsze - 2];


		// apply gain to HF component only

		_gain = k ? _HF_GAIN_ORDER_X : _HF_GAIN_ORDER_0;

		for (p = 0; p < _bsze; p++) _i0->_stth[p] *= _gain;


		// merge LF and HF components back into the original array

		for (p = 0; p < _bsze; p++) _data[k][p] = _i0->_sttl[p] - _i0->_stth[p];
	}

	return;
}

int	_vsappbbx_get_version(int *_major_version, int *_minor_version, int *_release_date) { if (_major_version); else return(_VSAPPBBX_NULL_ARGUMENT); if (_minor_version); else return(_VSAPPBBX_NULL_ARGUMENT); if (_release_date); else return(_VSAPPBBX_NULL_ARGUMENT); *_major_version = _VSAPPBBX_VERSN_MAJOR; *_minor_version = _VSAPPBBX_VERSN_MINOR; *_release_date = _VSAPPBBX_RELEASEDATE; return(_VSAPPBBX_NO_ERROR); }

int	_vsappbbx_get_timestamp(char *_s) { if (_s); else return(_VSAPPBBX_NULL_ARGUMENT); strcpy(_s, "build date "); strcat(_s, __DATE__); strcat(_s, " "); strcat(_s, __TIME__); return(_VSAPPBBX_NO_ERROR); }

int	_vsappbbx_init(void **_hndl, int _plfq, int _bsze, int _nchn, void *_hrtf, void *_xdta, const unsigned char *_fkey) {
	struct _vsappbbx_istate *_i0; int _d; int l; int p;

	if (_hndl); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_fkey); else return(_VSAPPBBX_NULL_ARGUMENT);

	if (_MIN_ALLOWED_PFREQUENCY <= _plfq && _plfq <= _MAX_ALLOWED_PFREQUENCY); else return(_VSAPPBBX_INVALID_FREQUENCY);

	if (_bsze % 2) return(_VSAPPBBX_INVALID_BLOCKSIZE);

	if (_nchn == 4 || _nchn == 9 || _nchn == 16 || _nchn == 25 || _nchn == 36 || _nchn == 49 || _nchn == 64); else return(_VSAPPBBX_UNSUPPORTED_SSETUP);

	_i0 = (struct _vsappbbx_istate *)malloc(sizeof(struct _vsappbbx_istate)); if (_i0); else return(_VSAPPBBX_MALLOC_FAILURE);

	_i0->_dsfq = _plfq; _i0->_anch = _nchn; _i0->_xord = _CHANNEL_XORDER_FUMA; _i0->_wght = _CHANNEL_WEIGHT_FUMA; _i0->_uoom = 0; _i0->_xhdl = 0; _i0->_sflt = 0;

	_i0->_inm2 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_inm2); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_inm2, 0, _i0->_anch * sizeof(float));
	_i0->_inm1 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_inm1); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_inm1, 0, _i0->_anch * sizeof(float));
	_i0->_lnm2 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_lnm2); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_lnm2, 0, _i0->_anch * sizeof(float));
	_i0->_lnm1 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_lnm1); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_lnm1, 0, _i0->_anch * sizeof(float));
	_i0->_hnm2 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_hnm2); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_hnm2, 0, _i0->_anch * sizeof(float));
	_i0->_hnm1 = (float *)malloc(_i0->_anch * sizeof(float)); if (_i0->_hnm1); else return(_VSAPPBBX_MALLOC_FAILURE); memset(_i0->_hnm1, 0, _i0->_anch * sizeof(float));

	_d = _process_xdta(_i0, _xdta); if (_d) return(_d); _setup_bandsplit(_i0, _XOVER_FREQUENCY, _plfq);

	_d = _establish_speaker_arrangement(_i0, _xdta); if (_d) return(_d);

	if (_i0->_xhdl) if (_hrtf); else return(_VSAPPBBX_XT_HRTF_NULL);

	_i0->_h0 = (struct _hrtf_data *)malloc(sizeof(struct _hrtf_data)); if (_i0->_h0); else return(_VSAPPBBX_MALLOC_FAILURE);

	_i0->_f0 = (struct _hrtf_hash *)malloc(sizeof(struct _hrtf_hash)); if (_i0->_f0); else return(_VSAPPBBX_MALLOC_FAILURE);

	if (_vsengine_prepare_hrtf(_i0->_xhdl ? _hrtf : _CP048_HRTF, _i0->_xhdl ? _i0->_xhdl : _CP048_HRTF_LENGTH, _plfq, 1, _i0->_h0, _fkey, NULL, NULL)) return(_VSAPPBBX_INIT_H_FAILURE);

	if (_vsengine_prepare_hrtf_hash(_i0->_h0, _bsze, _i0->_f0, NULL)) return(_VSAPPBBX_INIT_F_FAILURE);

	_i0->_h0->_head_radius = _DEFAULT_HEAD_RADIUS; // all these are not used anyway b/c playback is done via non-HQ mode of vsengine
	_i0->_h0->_trso_radius = _DEFAULT_TRSO_RADIUS; // but let's set them b/c at some point we might want to switch to HQ mode... 
	_i0->_h0->_neck_height = _DEFAULT_NECK_HEIGHT;
	_i0->_h0->_alpha_min = 0.100;
	_i0->_h0->_theta_min = 2.618; // 5/6 * pi
	_i0->_h0->_trso_rflct = 0.300;


	if (_compute_weight_matrix(_i0)) return(_VSAPPBBX_INTERNAL_FAILURE);


	_i0->_s0 = (struct _source_information **)malloc(_i0->_nsrc * sizeof(struct _source_information *)); if (_i0->_s0); else return(_VSAPPBBX_MALLOC_FAILURE);

	for (l = 0; l < _i0->_nsrc; l++) _i0->_s0[l] = (struct _source_information *)malloc(sizeof(struct _source_information));

	for (l = 0; l < _i0->_nsrc; l++) if (_i0->_s0[l]); else return(_VSAPPBBX_MALLOC_FAILURE);


	_i0->_w0 = (struct _source_work_area **)malloc(_i0->_nsrc * sizeof(struct _source_work_area *)); if (_i0->_w0); else return(_VSAPPBBX_MALLOC_FAILURE);

	for (l = 0; l < _i0->_nsrc; l++) _i0->_w0[l] = (struct _source_work_area *)malloc(sizeof(struct _source_work_area));

	for (l = 0; l < _i0->_nsrc; l++) if (_i0->_w0[l]); else return(_VSAPPBBX_MALLOC_FAILURE);


	for (l = 0; l < _i0->_nsrc; l++) { memset(_i0->_s0[l], 0, sizeof(struct _source_information)); _i0->_s0[l]->_data_chunk_size = _bsze; _i0->_s0[l]->_rir_length = 16 * _bsze; _i0->_s0[l]->_rir_realtime_order = 1; _i0->_s0[l]->_rir_fixdtail_order = 1; _i0->_s0[l]->_rir_maxallwd_order = 1; _i0->_s0[l]->_roomxe_size[0] = _ONEPARSEC; _i0->_s0[l]->_roomxe_size[1] = _ONEPARSEC; _i0->_s0[l]->_roomxe_size[2] = _ONEPARSEC; _i0->_s0[l]->_intpst_blend_len = _BLEND_LEN; }


	for (l = 0; l < _i0->_nsrc; l++) if (_vsengine_prepare_work_area(_i0->_s0[l], _i0->_w0[l], _i0->_h0, NULL)) return(_VSAPPBBX_INIT_W_FAILURE);

	for (l = 0; l < _i0->_nsrc; l++) if (_vsengine_setup_source(_i0->_s0[l], _i0->_w0[l], _i0->_h0, NULL, 0)) return(_VSAPPBBX_INIT_S_FAILURE);


	if (_i0->_uoom) { /* set up source and work area for tail */

					  /* everything for the tail is set up as per Akita Blue tuning */
					  /* EXCEPT that reflection coefficients are larger by 0.0725 */
					  /* and LWO suppression mode 1 is used */

		_i0->_t0 = (struct _source_information *)malloc(sizeof(struct _source_information)); if (_i0->_t0); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_u0 = (struct _source_work_area   *)malloc(sizeof(struct _source_work_area)); if (_i0->_u0); else return(_VSAPPBBX_MALLOC_FAILURE);

		_i0->_t0->_data_chunk_size = _bsze;
		_i0->_t0->_rir_realtime_order = 1;
		_i0->_t0->_rir_fixdtail_order = 32;
		_i0->_t0->_rir_maxallwd_order = 32;
		_i0->_t0->_rir_length = 32768;
		_i0->_t0->_highquality_mode = 1;
		_i0->_t0->_interpolate_hrtf = 0;
		_i0->_t0->_randomize_reverb = 1;
		_i0->_t0->_xrelativity_mode = 0;
		_i0->_t0->_ffdirection_enbl = 0;
		_i0->_t0->_ffdirection_angl = 0;
		_i0->_t0->_distance_lqlimit = 0.0;
		_i0->_t0->_source_range_min = 0.0;
		_i0->_t0->_source_range_max = _ONEPARSEC;
		_i0->_t0->_source_range_asp = 0;
		_i0->_t0->_source_decay_cut = 0;
		_i0->_t0->_intpst_blend_len = 0;
		_i0->_t0->_use_overlap_nadd = 0;

		_i0->_t0->_roomxe_size[0] = 4.060; _i0->_t0->_roomxe_size[1] = 5.510; _i0->_t0->_roomxe_size[2] = 2.900;
		_i0->_t0->_source_ixyz[0] = 0.116; _i0->_t0->_source_ixyz[1] = -1.370; _i0->_t0->_source_ixyz[2] = 0.175;
		_i0->_t0->_listnr_ixyz[0] = 0.091; _i0->_t0->_listnr_ixyz[1] = -0.620; _i0->_t0->_listnr_ixyz[2] = 0.100;
		_i0->_t0->_listnr_iypr[0] = 0.000; _i0->_t0->_listnr_iypr[1] = 0.000; _i0->_t0->_listnr_iypr[2] = 0.000;
		_i0->_t0->_listnr_dxyz[0] = 0.000; _i0->_t0->_listnr_dxyz[1] = 0.000; _i0->_t0->_listnr_dxyz[2] = 0.000;
		_i0->_t0->_listnr_dypr[0] = 0.000; _i0->_t0->_listnr_dypr[1] = 0.000; _i0->_t0->_listnr_dypr[2] = 0.000;
		_i0->_t0->_offset_ixyz[0] = 0.000; _i0->_t0->_offset_ixyz[1] = 0.000; _i0->_t0->_offset_ixyz[2] = 0.000;

		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[0][p] = 0.825; // 0.75
		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[1][p] = 0.825; // 0.75
		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[2][p] = 0.525; // 0.45
		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[3][p] = 0.925; // 0.85
		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[4][p] = 0.625; // 0.55
		for (p = 0; p < _VSENGINE_RFNBANDS; p++) _i0->_t0->_rflctn_coef[5][p] = 0.725; // 0.65

		for (p = 0; p < _VSENGINE_MAXRFORDER; p++) _i0->_t0->_rfomlf_coef[p] = 1.000;

		_i0->_t0->_rfomlf_coef[0] = 0.0000;
		_i0->_t0->_rfomlf_coef[1] = 0.5012;
		_i0->_t0->_rfomlf_coef[2] = 0.7079;

		_i0->_t0->_rshape_polyhedral = NULL;
		_i0->_t0->_ugoccluder_xchain = NULL;

		if (_vsengine_prepare_work_area(_i0->_t0, _i0->_u0, _i0->_h0, NULL)) return(_VSAPPBBX_INIT_U_FAILURE);

		if (_vsengine_setup_source(_i0->_t0, _i0->_u0, _i0->_h0, NULL, 0)) return(_VSAPPBBX_INIT_T_FAILURE);
	}


	_i0->_ixdx = (float *)malloc(_bsze * sizeof(float)); if (_i0->_ixdx); else return(_VSAPPBBX_MALLOC_FAILURE);
	_i0->_tdta = (float *)malloc(_bsze * sizeof(float)); if (_i0->_tdta); else return(_VSAPPBBX_MALLOC_FAILURE);
	_i0->_oxtl = (float *)malloc(_bsze * sizeof(float)); if (_i0->_oxtl); else return(_VSAPPBBX_MALLOC_FAILURE);
	_i0->_oxtr = (float *)malloc(_bsze * sizeof(float)); if (_i0->_oxtr); else return(_VSAPPBBX_MALLOC_FAILURE);
	_i0->_sttl = (float *)malloc(_bsze * sizeof(float)); if (_i0->_sttl); else return(_VSAPPBBX_MALLOC_FAILURE);
	_i0->_stth = (float *)malloc(_bsze * sizeof(float)); if (_i0->_stth); else return(_VSAPPBBX_MALLOC_FAILURE);

	_i0->_mcok = _VSAPPBBX_MAGICNUMBER; *_hndl = (void *)_i0; return(_VSAPPBBX_NO_ERROR);
}

int	_vsappbbx_vlsn(void **_hndl, int *_pout) {
	struct _vsappbbx_istate *_i0;

	if (_hndl); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_pout); else return(_VSAPPBBX_NULL_ARGUMENT);

	_i0 = *(struct _vsappbbx_istate **)_hndl; if (_i0); else return(_VSAPPBBX_INVALID_HANDLE); if (_i0->_mcok == _VSAPPBBX_MAGICNUMBER); else return(_VSAPPBBX_DEFECTIVE_HANDLE);

	*_pout = _i0->_nsrc; return(_VSAPPBBX_NO_ERROR);
}

int	_vsappbbx_vlsd(void **_hndl, double *_pdta, float **_inpx, float **_outx) {
	struct _vsappbbx_istate *_i0; int l, k, p; int _bsze;

	if (_hndl); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_pdta); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_inpx); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_outx); else return(_VSAPPBBX_NULL_ARGUMENT);

	_i0 = *(struct _vsappbbx_istate **)_hndl; if (_i0); else return(_VSAPPBBX_INVALID_HANDLE); if (_i0->_mcok == _VSAPPBBX_MAGICNUMBER); else return(_VSAPPBBX_DEFECTIVE_HANDLE);

	for (k = 0; k < _i0->_anch; k++) if (_inpx[k]); else return(_VSAPPBBX_NULL_SUBARGUMENT);

	for (l = 0; l < _i0->_nsrc; l++) if (_outx[l]); else return(_VSAPPBBX_NULL_SUBARGUMENT);

	if (_i0->_sflt) _split_filter_combine(_i0, _inpx); _update_position(_i0, _pdta); _bsze = _i0->_s0[0]->_data_chunk_size; for (l = 0; l < _i0->_nsrc; l++) memset(_outx[l], 0, _bsze * sizeof(float));

	for (l = 0; l < _i0->_nsrc; l++) {
		memset(_i0->_ixdx, 0, _bsze * sizeof(float));

		for (k = 0; k < _i0->_anch; k++)
			for (p = 0; p < _bsze; p++)
				_i0->_ixdx[p] += _i0->_pw[l][k] * _inpx[k][p];

		memcpy(_outx[l], _i0->_ixdx, _bsze * sizeof(float));
	}

	return(_VSAPPBBX_NO_ERROR);
}

int	_vsappbbx_baud(void **_hndl, double *_pdta, float **_inpx, float **_outx) {
	struct _vsappbbx_istate *_i0; int l, k, p; int _bsze;

	if (_hndl); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_pdta); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_inpx); else return(_VSAPPBBX_NULL_ARGUMENT);
	if (_outx); else return(_VSAPPBBX_NULL_ARGUMENT);

	_i0 = *(struct _vsappbbx_istate **)_hndl; if (_i0); else return(_VSAPPBBX_INVALID_HANDLE); if (_i0->_mcok == _VSAPPBBX_MAGICNUMBER); else return(_VSAPPBBX_DEFECTIVE_HANDLE);

	for (k = 0; k < _i0->_anch; k++) if (_inpx[k]); else return(_VSAPPBBX_NULL_SUBARGUMENT);

	for (l = 0; l < 2; l++) if (_outx[l]); else return(_VSAPPBBX_NULL_SUBARGUMENT);

	if (_i0->_sflt) _split_filter_combine(_i0, _inpx); _update_position(_i0, _pdta); _bsze = _i0->_s0[0]->_data_chunk_size; for (l = 0; l < 2; l++) memset(_outx[l], 0, _bsze * sizeof(float)); memset(_i0->_tdta, 0, _bsze * sizeof(float));

	for (l = 0; l < _i0->_nsrc; l++) {
		memset(_i0->_ixdx, 0, _bsze * sizeof(float));

		for (k = 0; k < _i0->_anch; k++)
			for (p = 0; p < _bsze; p++)
				_i0->_ixdx[p] += _i0->_pw[l][k] * _inpx[k][p];

		for (k = 0; k < _i0->_anch; k++)
			for (p = 0; p < _bsze; p++)
				_i0->_tdta[p] += _i0->_pw[l][k] * _inpx[k][p];

		if (_vsengine_submit_idata(_i0->_s0[l], _i0->_w0[l], _i0->_ixdx)) return(_VSAPPBBX_RENDER_ERROR_TYPE_A1);

		if (_vsengine_produce_support(_i0->_s0[l], _i0->_w0[l])) return(_VSAPPBBX_RENDER_ERROR_TYPE_B1);

		if (_vsengine_produce_odata(_i0->_s0[l], _i0->_w0[l], _i0->_h0, _i0->_f0, _i0->_oxtl, _i0->_oxtr)) return(_VSAPPBBX_RENDER_ERROR_TYPE_C1);

		for (p = 0; p < _bsze; p++) _outx[0][p] += _i0->_oxtl[p];
		for (p = 0; p < _bsze; p++) _outx[1][p] += _i0->_oxtr[p];
	}

	if (_i0->_uoom) if (_vsengine_submit_idata(_i0->_t0, _i0->_u0, _i0->_tdta)) return(_VSAPPBBX_RENDER_ERROR_TYPE_A2);

	if (_i0->_uoom) if (_vsengine_produce_support(_i0->_t0, _i0->_u0)) return(_VSAPPBBX_RENDER_ERROR_TYPE_B2);

	if (_i0->_uoom) if (_vsengine_produce_odata(_i0->_t0, _i0->_u0, _i0->_h0, NULL, _i0->_oxtl, _i0->_oxtr)) return(_VSAPPBBX_RENDER_ERROR_TYPE_C2);

	if (_i0->_uoom) for (p = 0; p < _bsze; p++) _outx[0][p] += _i0->_oxtl[p];
	if (_i0->_uoom) for (p = 0; p < _bsze; p++) _outx[1][p] += _i0->_oxtr[p];

	return(_VSAPPBBX_NO_ERROR);
}

int	_vsappbbx_zout(void **_hndl) {
	struct _vsappbbx_istate *_i0; int l;

	_i0 = *(struct _vsappbbx_istate **)_hndl; if (_i0); else return(_VSAPPBBX_INVALID_HANDLE); if (_i0->_mcok == _VSAPPBBX_MAGICNUMBER); else return(_VSAPPBBX_DEFECTIVE_HANDLE);

	for (l = 0; l < _i0->_nsrc; l++) _vsengine_flush_source(_i0->_s0[l], _i0->_w0[l]);

	memset(_i0->_inm2, 0, _i0->_anch * sizeof(float));
	memset(_i0->_inm1, 0, _i0->_anch * sizeof(float));

	return(_VSAPPBBX_NO_ERROR);
}

int	_vsappbbx_exit(void **_hndl) {
	struct _vsappbbx_istate *_i0; int l;

	_i0 = *(struct _vsappbbx_istate **)_hndl; if (_i0); else return(_VSAPPBBX_INVALID_HANDLE); if (_i0->_mcok == _VSAPPBBX_MAGICNUMBER); else return(_VSAPPBBX_DEFECTIVE_HANDLE);

	for (l = 0; l < _i0->_nsrc; l++) _vsengine_cleanup_work_area(_i0->_s0[l], _i0->_w0[l]);

	if (_i0->_uoom) _vsengine_cleanup_work_area(_i0->_t0, _i0->_u0);

	_vsengine_cleanup_hrtf_hash(_i0->_f0);

	_vsengine_cleanup_hrtf(_i0->_h0);

	for (l = 0; l < _i0->_nsrc; l++) free(_i0->_w0[l]); free(_i0->_w0); if (_i0->_uoom) free(_i0->_u0);

	for (l = 0; l < _i0->_nsrc; l++) free(_i0->_s0[l]); free(_i0->_s0); if (_i0->_uoom) free(_i0->_t0);

	free(_i0->_f0); free(_i0->_h0);

	free(_i0->_inm2);
	free(_i0->_inm1);
	free(_i0->_lnm2);
	free(_i0->_lnm1);
	free(_i0->_hnm2);
	free(_i0->_hnm1);

	free(_i0->_stht);
	free(_i0->_spsi);

	free(_i0->_ixdx);
	free(_i0->_tdta);
	free(_i0->_oxtl);
	free(_i0->_oxtr);
	free(_i0->_sttl);
	free(_i0->_stth);

	*_hndl = NULL; _i0->_mcok = 0; free(_i0); return(_VSAPPBBX_NO_ERROR);
}