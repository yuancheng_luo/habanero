/* This is the header file for VisiSonics Spatial Engine DLL (4.5.20160714) */

/* ATTENTION: you must #include "fftw3.h" before including this file */

#ifndef __VSENGINE_H__
#define __VSENGINE_H__

#define	_VSENGINE_NO_ERROR		0

#define	_VSENGINE_BASE_ERROR_NUMBER	-16639

#define	_VSENGINE_INTERNAL_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  1)
#define	_VSENGINE_INVALID_PARAMETER	(_VSENGINE_BASE_ERROR_NUMBER -  2)
#define	_VSENGINE_MALLOC_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  3)
#define	_VSENGINE_EXEC_PLAN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  4)
#define	_VSENGINE_HRTF_OPEN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  5)
#define	_VSENGINE_HRTF_READ_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  6)
#define	_VSENGINE_HRTF_HECT_EXCESS	(_VSENGINE_BASE_ERROR_NUMBER -  7)
#define	_VSENGINE_INIT_REQUIRED		(_VSENGINE_BASE_ERROR_NUMBER -  8)
#define	_VSENGINE_ILLEGAL_UPDATE	(_VSENGINE_BASE_ERROR_NUMBER -  9)
#define	_VSENGINE_SRCC_OUT_OF_RANGE	(_VSENGINE_BASE_ERROR_NUMBER - 10)
#define	_VSENGINE_UNUSED_ERRORCODE_A	(_VSENGINE_BASE_ERROR_NUMBER - 11)
#define	_VSENGINE_INVALID_POLYGON	(_VSENGINE_BASE_ERROR_NUMBER - 12)
#define	_VSENGINE_INVALID_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 13)
#define	_VSENGINE_INVALID_PLATFORM	(_VSENGINE_BASE_ERROR_NUMBER - 14)
#define	_VSENGINE_NACTIVE_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 15)
#define	_VSENGINE_EXPIRED_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 16)
#define	_VSENGINE_HRTF_IFAILURE		(_VSENGINE_BASE_ERROR_NUMBER - 17)
#define	_VSENGINE_HRTF_DATA_DAMAGE	(_VSENGINE_BASE_ERROR_NUMBER - 18)
#define	_VSENGINE_HRTF_FREQ_MISMATCH	(_VSENGINE_BASE_ERROR_NUMBER - 19)
#define	_VSENGINE_HRTF_FORMAT_UNSUPP	(_VSENGINE_BASE_ERROR_NUMBER - 20)
#define	_VSENGINE_HRTF_DATA_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 21)
#define	_VSENGINE_HRTF_HASH_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 22)
#define	_VSENGINE_WORK_AREA_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 23)
#define	_VSENGINE_NOT_A_NUMBER		(_VSENGINE_BASE_ERROR_NUMBER - 24)
#define	_VSENGINE_NULL_ARGUMENT		(_VSENGINE_BASE_ERROR_NUMBER - 25)
#define	_VSENGINE_HRTF_FCNT_EXCESS	(_VSENGINE_BASE_ERROR_NUMBER - 26)
#define	_VSENGINE_INVALID_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 27)
#define	_VSENGINE_DEGENERATE_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 28)
#define	_VSENGINE_NONPLANAR_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 29)
#define	_VSENGINE_INVALID_BLOCK_SIZE	(_VSENGINE_BASE_ERROR_NUMBER - 30)
#define	_VSENGINE_UNUSED_ERRORCODE_B	(_VSENGINE_BASE_ERROR_NUMBER - 31)
#define	_VSENGINE_UNUSED_ERRORCODE_C	(_VSENGINE_BASE_ERROR_NUMBER - 32)
#define	_VSENGINE_UNSUITABLE_HASH	(_VSENGINE_BASE_ERROR_NUMBER - 33)
#define	_VSENGINE_UNUSED_ERRORCODE_D	(_VSENGINE_BASE_ERROR_NUMBER - 34)

#define	_VSENGINE_OCCLPMAX		16
#define	_VSENGINE_RFNBANDS		48
#define	_VSENGINE_MAXRFORDER		128

struct _vecpnt { double _x[3]; int _pv; struct _vecpnt *_p_next; };

struct _bbvsrc {
	int	_level, _vsb_ok;
	struct	_vecpnt *_vspnt;
	struct	_bbface *_rface;
	struct	_bbvsrc *_sibling;
	struct	_bbvsrc *_children;
	struct	_bbvsrc *_parent;
	struct	_rflist *_flctrs; };

struct _bbface {
	int	_obstructive;
	struct	_vecpnt *_fnrm; double _fdxo;
	struct	_vecpnt *_p_list;
	struct	_bbface *_f_next;
	double	_attenuation[_VSENGINE_RFNBANDS]; };

struct _rflist { struct _bbface *_rfpntr; struct _rflist *_r_next; };

struct _ocugly { int _npnt; double _n[4]; float _tvalue;
	double _x[3][_VSENGINE_OCCLPMAX];
	double _u[3][_VSENGINE_OCCLPMAX];
	struct _ocugly *_ocnext; };

struct _hrtf_data { int _malloc_ok; int _malloc_nt;
	int	_pnum;
	int	_hsfq;
	int	_hlen;
	int	_hect;
	short	*_hash;
	double	*_tht, *_psi;
	float	**_hrir_l, **_hrir_r;
	double	_head_radius;
	double	_trso_radius;
	double	_neck_height;
	double	_alpha_min;
	double	_theta_min;
	double	_trso_rflct;
	int	_xtrfield_a;
	int	_xtrfield_b;
	int	_xtrfield_c;
	int	_xtrfield_d;
	int	_validation; };

struct _source_information {
	int	_data_chunk_size;
	int	_rir_realtime_order;
	int	_rir_fixdtail_order;
	int	_rir_maxallwd_order;
	int	_rir_length;
	int	_highquality_mode;
	int	_interpolate_hrtf;
	int	_randomize_reverb;
	int	_xrelativity_mode;
	int	_ffdirection_enbl;
	double	_ffdirection_angl;
	double	_distance_lqlimit;
	double	_source_range_min;
	double	_source_range_max;
	int	_source_range_asp;
	int	_source_decay_cut;
	int	_intpst_blend_len;
	int	_use_overlap_nadd;
	double	_roomxe_size[3];
	double	_source_ixyz[3];
	double	_listnr_ixyz[3];
	double	_listnr_iypr[3];
	double	_listnr_dxyz[3];
	double	_listnr_dypr[3];
	double	_offset_ixyz[3];
	double	_rflctn_coef[6][_VSENGINE_RFNBANDS];
	double	_rfomlf_coef   [_VSENGINE_MAXRFORDER];
	struct	_bbface *_rshape_polyhedral;
	struct	_ocugly *_ugoccluder_xchain; };

struct _source_work_area { int _malloc_ok, _malloc_nt; int _sp1, _sp2, _sp3, _sp4, _sp5, _sp6, _sp7, _nblk, _nblx, _bnpt, _bsze, _htsn, _htst, _lppp; int _pxcall_ivld, _rngl_output, _rngl_xxseed, _rngl_iiseed, _rngl_buffer[32]; void *_pf01, *_pb23, *_pb45, *_pf67, *_pb67, *_pf89, *_pb89, *_psff, *_psfx, *_psbs, *_psbf; float *_hwnd, *_dwnd, *_flut, *_pblk, *_lbbl, *_lbbr, *_mbbl, *_mbbr, *_rtdl, *_rtdr, *_qcxl, *_qcxr, *_qczl, *_qczr, *_pcxl, *_pcxr, *_pczl, *_pczr, *_ccxl, *_ccxr, *_cczl, *_cczr, *_oper_left, *_oper_rght, *_ibuf_left, *_ibuf_rght, *_scratch00, *_response0, *_ixbdta; double _dcpy, _mdst, _srrr; double *_lposdata, *_iposdata, *_cposdata;

	fftwf_complex	**_ibdq, **_rirx, **_dtaw, **_dtax, **_dtay, **_dtaz, **_pfdata; unsigned char *_bigtable; fftwf_complex *_cx0, *_cx1, *_bdx, *_b0s, *_b0f, *_b1s, *_b1f, *_lblock; double _pxcall_ixyz[3]; double _cxcall_ixyz[3];

	struct _bbvsrc	*_vsrc_tree, *_vsrc_chain; };

struct _hrtf_hash { int _malloc_ok, _malloc_nt; int _sp1, _sp2, _sp3, _sp4; fftwf_complex ***_hrtf_x; };

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

int _vsengine_get_version(int *_major_version, int *_minor_version, int *_release_date);
int _vsengine_get_timestamp(char *_string_buffer);

int _vsengine_init(int *_handle);

int _vsengine_verify_hrtf_data(struct _hrtf_data *_h0);
int _vsengine_verify_hrtf_hash(struct _hrtf_hash *_f0);
int _vsengine_verify_work_area(struct _source_work_area *_w0);

int _vsengine_query_hrtf_mempool_size(const unsigned char *_hrtf_file_content, int _hrtf_file_length, int _playback_frequency, int _do_resample, int *_size_answer);
int _vsengine_prepare_hrtf(const unsigned char *_hrtf_file_content, int _hrtf_file_length, int _playback_frequency, int _do_resample, struct _hrtf_data *_h0, const unsigned char *_fk, unsigned char *_mempool, int *_days_remaining);

int _vsengine_query_hrtf_hash_mempool_size(struct _hrtf_data *_h0, int _block_size, int *_size_answer);
int _vsengine_prepare_hrtf_hash(struct _hrtf_data *_h0, int _block_size, struct _hrtf_hash *_f0, unsigned char *_mempool);

int _vsengine_query_work_area_mempool_size(struct _source_information *_s0, struct _hrtf_data *_h0, int *_size_answer);
int _vsengine_prepare_work_area(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, unsigned char *_mempool);

int _vsengine_assign_normal(struct _bbface *_face);

int _vsengine_check_occluder(struct _ocugly *_o0);

int _vsengine_setup_source(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, unsigned char *_tailhmap, int _tailhmaplength);

int _vsengine_flush_source(struct _source_information *_s0, struct _source_work_area *_w0);

int _vsengine_submit_idata(struct _source_information *_s0, struct _source_work_area *_w0, const float *_inp_block);
int _vsengine_produce_support(struct _source_information *_s0, struct _source_work_area *_w0);
int _vsengine_produce_odata(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, struct _hrtf_hash *_f0, float *_out_block_left, float *_out_block_rght);

int _vsengine_cleanup_work_area(struct _source_information *_s0, struct _source_work_area *_w0);
int _vsengine_cleanup_hrtf_hash(struct _hrtf_hash *_f0);
int _vsengine_cleanup_hrtf(struct _hrtf_data *_h0);

int _vsengine_exit(int *_handle);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* __VSENGINE_H__ */




//  /* This is the header file for VisiSonics Spatial Engine DLL (4.6.20160831) */
//
//  /* ATTENTION: you must #include "fftw3.h" before including this file */
//
//#ifndef __VSENGINE_H__
//#define __VSENGINE_H__
//
//#include "fftw3.h"
//
//#define	_VSENGINE_NO_ERROR		0
//
//#define	_VSENGINE_BASE_ERROR_NUMBER	-16639
//
//#define	_VSENGINE_INTERNAL_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  1)
//#define	_VSENGINE_INVALID_PARAMETER	(_VSENGINE_BASE_ERROR_NUMBER -  2)
//#define	_VSENGINE_MALLOC_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  3)
//#define	_VSENGINE_EXEC_PLAN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  4)
//#define	_VSENGINE_HRTF_OPEN_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  5)
//#define	_VSENGINE_HRTF_READ_FAILURE	(_VSENGINE_BASE_ERROR_NUMBER -  6)
//#define	_VSENGINE_HRTF_HECT_EXCESS	(_VSENGINE_BASE_ERROR_NUMBER -  7)
//#define	_VSENGINE_INIT_REQUIRED		(_VSENGINE_BASE_ERROR_NUMBER -  8)
//#define	_VSENGINE_ILLEGAL_UPDATE	(_VSENGINE_BASE_ERROR_NUMBER -  9)
//#define	_VSENGINE_SRCC_OUT_OF_RANGE	(_VSENGINE_BASE_ERROR_NUMBER - 10)
//#define	_VSENGINE_UNUSED_ERRORCODE_A	(_VSENGINE_BASE_ERROR_NUMBER - 11)
//#define	_VSENGINE_INVALID_POLYGON	(_VSENGINE_BASE_ERROR_NUMBER - 12)
//#define	_VSENGINE_INVALID_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 13)
//#define	_VSENGINE_INVALID_PLATFORM	(_VSENGINE_BASE_ERROR_NUMBER - 14)
//#define	_VSENGINE_NACTIVE_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 15)
//#define	_VSENGINE_EXPIRED_LICENSE	(_VSENGINE_BASE_ERROR_NUMBER - 16)
//#define	_VSENGINE_HRTF_IFAILURE		(_VSENGINE_BASE_ERROR_NUMBER - 17)
//#define	_VSENGINE_HRTF_DATA_DAMAGE	(_VSENGINE_BASE_ERROR_NUMBER - 18)
//#define	_VSENGINE_HRTF_FREQ_MISMATCH	(_VSENGINE_BASE_ERROR_NUMBER - 19)
//#define	_VSENGINE_HRTF_FORMAT_UNSUPP	(_VSENGINE_BASE_ERROR_NUMBER - 20)
//#define	_VSENGINE_HRTF_DATA_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 21)
//#define	_VSENGINE_HRTF_HASH_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 22)
//#define	_VSENGINE_WORK_AREA_NONINIT	(_VSENGINE_BASE_ERROR_NUMBER - 23)
//#define	_VSENGINE_NOT_A_NUMBER		(_VSENGINE_BASE_ERROR_NUMBER - 24)
//#define	_VSENGINE_NULL_ARGUMENT		(_VSENGINE_BASE_ERROR_NUMBER - 25)
//#define	_VSENGINE_HRTF_FCNT_EXCESS	(_VSENGINE_BASE_ERROR_NUMBER - 26)
//#define	_VSENGINE_INVALID_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 27)
//#define	_VSENGINE_DEGENERATE_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 28)
//#define	_VSENGINE_NONPLANAR_OCCLUDER	(_VSENGINE_BASE_ERROR_NUMBER - 29)
//#define	_VSENGINE_INVALID_BLOCK_SIZE	(_VSENGINE_BASE_ERROR_NUMBER - 30)
//#define	_VSENGINE_UNUSED_ERRORCODE_B	(_VSENGINE_BASE_ERROR_NUMBER - 31)
//#define	_VSENGINE_UNUSED_ERRORCODE_C	(_VSENGINE_BASE_ERROR_NUMBER - 32)
//#define	_VSENGINE_UNSUITABLE_HASH	(_VSENGINE_BASE_ERROR_NUMBER - 33)
//#define	_VSENGINE_UNUSED_ERRORCODE_D	(_VSENGINE_BASE_ERROR_NUMBER - 34)
//
//#define	_VSENGINE_OCCLPMAX		16
//#define	_VSENGINE_RFNBANDS		48
//#define	_VSENGINE_MAXRFORDER		128
//
//struct _vecpnt { double _x[3]; int _pv; struct _vecpnt *_p_next; };
//
//struct _bbvsrc {
//	int	_level, _vsb_ok;
//	struct	_vecpnt *_vspnt;
//	struct	_bbface *_rface;
//	struct	_bbvsrc *_sibling;
//	struct	_bbvsrc *_children;
//	struct	_bbvsrc *_parent;
//	struct	_rflist *_flctrs;
//};
//
//struct _bbface {
//	int	_obstructive;
//	struct	_vecpnt *_fnrm; double _fdxo;
//	struct	_vecpnt *_p_list;
//	struct	_bbface *_f_next;
//	double	_attenuation[_VSENGINE_RFNBANDS];
//};
//
//struct _rflist { struct _bbface *_rfpntr; struct _rflist *_r_next; };
//
//struct _ocugly {
//	int _npnt; double _n[4]; float _tvalue;
//	double _x[3][_VSENGINE_OCCLPMAX];
//	double _u[3][_VSENGINE_OCCLPMAX];
//	struct _ocugly *_ocnext;
//};
//
//struct _hrtf_data {
//	int _malloc_ok; int _malloc_nt;
//	int	_pnum;
//	int	_hsfq;
//	int	_hlen;
//	int	_hect;
//	int	_tdcl;
//	short	*_hash;
//	double	*_tht, *_psi;
//	float	 *_hdly_l, *_hdly_r;
//	float	**_hrir_l, **_hrir_r;
//	float	**_hcut_l, **_hcut_r;
//	double	_head_radius;
//	double	_trso_radius;
//	double	_neck_height;
//	double	_alpha_min;
//	double	_theta_min;
//	double	_trso_rflct;
//	int	_xtrfield_a;
//	int	_xtrfield_b;
//	int	_xtrfield_c;
//	int	_xtrfield_d;
//	int	_validation;
//};
//
//struct _source_information {
//	int	_data_chunk_size;
//	int	_rir_realtime_order;
//	int	_rir_fixdtail_order;
//	int	_rir_maxallwd_order;
//	int	_rir_length;
//	int	_highquality_mode;
//	int	_interpolate_hrtf;
//	int	_randomize_reverb;
//	int	_xrelativity_mode;
//	int	_ffdirection_enbl;
//	double	_ffdirection_angl;
//	double	_distance_lqlimit;
//	double	_source_range_min;
//	double	_source_range_max;
//	int	_source_range_asp;
//	int	_source_decay_cut;
//	int	_intpst_blend_len;
//	int	_smooth_blend_mod;
//	double	_roomxe_size[3];
//	double	_source_ixyz[3];
//	double	_listnr_ixyz[3];
//	double	_listnr_iypr[3];
//	double	_listnr_dxyz[3];
//	double	_listnr_dypr[3];
//	double	_offset_ixyz[3];
//	double	_rflctn_coef[6][_VSENGINE_RFNBANDS];
//	double	_rfomlf_coef[_VSENGINE_MAXRFORDER];
//	double	_tailml_coef;
//	struct	_bbface *_rshape_polyhedral;
//	struct	_ocugly *_ugoccluder_xchain;
//};
//
//struct _source_work_area {
//	int _malloc_ok, _malloc_nt; int _sp1, _sp2, _sp3, _sp4, _sp5, _sp6, _hwii, _nblk, _nblx, _bnpt, _bsze, _htsn, _htst, _lppp, _dtxp, _icbp, _icpl; int _pxcall_ivld, _rngl_output, _rngl_xxseed, _rngl_iiseed, _rngl_buffer[32]; void *_pf01, *_pb23, *_pb45, *_pf67, *_pb67, *_pf89, *_pb89, *_psff, *_psfx, *_psbs, *_psbf; float *_hwnd, *_dwnd, *_flut, *_lbbl, *_lbbr, *_mbbl, *_mbbr, *_icpy, *_oper_left, *_oper_rght, *_ibuf_left, *_ibuf_rght, *_scratch00, *_response0; double _dcpy, _mdst, _srrr; double *_lposdata, *_tposdata, *_cposdata;
//
//	fftwf_complex	**_ibdq, **_rirx, **_dtax; fftwf_complex *_cx0, *_cx1, *_bdt, *_bdf, *_b0s, *_b0f, *_b1s, *_b1f; double _pxcall_ixyz[3], _cxcall_ixyz[3]; unsigned char *_bigtable;
//
//	struct _bbvsrc	*_vsrc_tree, *_vsrc_chain;
//};
//
//struct _hrtf_hash { int _malloc_ok, _malloc_nt; int _sp1, _sp2, _sp3, _sp4; fftwf_complex ***_hrtf_x; };
//
//#ifdef __cplusplus
//extern "C"
//{
//#endif /* __cplusplus */
//
//	int _vsengine_get_version(int *_major_version, int *_minor_version, int *_release_date);
//	int _vsengine_get_timestamp(char *_string_buffer);
//
//	int _vsengine_init(int *_handle);
//
//	int _vsengine_verify_hrtf_data(struct _hrtf_data *_h0);
//	int _vsengine_verify_hrtf_hash(struct _hrtf_hash *_f0);
//	int _vsengine_verify_work_area(struct _source_work_area *_w0);
//
//	int _vsengine_query_hrtf_mempool_size(const unsigned char *_hrtf_file_content, int _hrtf_file_length, int _playback_frequency, int _do_resample, int *_size_answer);
//	int _vsengine_prepare_hrtf(const unsigned char *_hrtf_file_content, int _hrtf_file_length, int _playback_frequency, int _do_resample, struct _hrtf_data *_h0, const unsigned char *_fk, unsigned char *_mempool, int *_days_remaining);
//
//	int _vsengine_query_hrtf_hash_mempool_size(struct _hrtf_data *_h0, int _block_size, int *_size_answer);
//	int _vsengine_prepare_hrtf_hash(struct _hrtf_data *_h0, int _block_size, struct _hrtf_hash *_f0, unsigned char *_mempool);
//
//	int _vsengine_query_work_area_mempool_size(struct _source_information *_s0, struct _hrtf_data *_h0, int *_size_answer);
//	int _vsengine_prepare_work_area(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, unsigned char *_mempool);
//
//	int _vsengine_assign_normal(struct _bbface *_face);
//
//	int _vsengine_check_occluder(struct _ocugly *_o0);
//
//	int _vsengine_setup_source(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, unsigned char *_tailhmap, int _tailhmaplength);
//
//	int _vsengine_flush_source(struct _source_information *_s0, struct _source_work_area *_w0);
//
//	int _vsengine_submit_idata(struct _source_information *_s0, struct _source_work_area *_w0, const float *_inp_block);
//	int _vsengine_produce_support(struct _source_information *_s0, struct _source_work_area *_w0);
//	int _vsengine_produce_odata(struct _source_information *_s0, struct _source_work_area *_w0, struct _hrtf_data *_h0, struct _hrtf_hash *_f0, float *_out_block_left, float *_out_block_rght);
//
//	int _vsengine_cleanup_work_area(struct _source_information *_s0, struct _source_work_area *_w0);
//	int _vsengine_cleanup_hrtf_hash(struct _hrtf_hash *_f0);
//	int _vsengine_cleanup_hrtf(struct _hrtf_data *_h0);
//
//	int _vsengine_exit(int *_handle);
//
//#ifdef __cplusplus
//} /* extern "C" */
//#endif /* __cplusplus */
//
//#endif /* __VSENGINE_H__ */