/* VisiSonics Ambisonics Pure Player Black Box library header (1.2.20160718) */

#ifndef __VSAPPBBX_H__
#define __VSAPPBBX_H__

#define	_VSAPPBBX_NO_ERROR		0

#define	_VSAPPBBX_BASE_ERROR_NUMBER	-17151

#define	_VSAPPBBX_INTERNAL_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER -  1)
#define	_VSAPPBBX_MALLOC_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER -  2)
#define	_VSAPPBBX_NULL_ARGUMENT		(_VSAPPBBX_BASE_ERROR_NUMBER -  3)
#define	_VSAPPBBX_NULL_SUBARGUMENT	(_VSAPPBBX_BASE_ERROR_NUMBER -  4)
#define	_VSAPPBBX_INVALID_FREQUENCY	(_VSAPPBBX_BASE_ERROR_NUMBER -  5)
#define	_VSAPPBBX_INVALID_BLOCKSIZE	(_VSAPPBBX_BASE_ERROR_NUMBER -  6)
#define	_VSAPPBBX_UNSUPPORTED_SSETUP	(_VSAPPBBX_BASE_ERROR_NUMBER -  7)
#define	_VSAPPBBX_INIT_H_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER -  8)
#define	_VSAPPBBX_INIT_F_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER -  9)
#define	_VSAPPBBX_INIT_W_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER - 10)
#define	_VSAPPBBX_INIT_S_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER - 11)
#define	_VSAPPBBX_INVALID_HANDLE	(_VSAPPBBX_BASE_ERROR_NUMBER - 12)
#define	_VSAPPBBX_DEFECTIVE_HANDLE	(_VSAPPBBX_BASE_ERROR_NUMBER - 13)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_A1	(_VSAPPBBX_BASE_ERROR_NUMBER - 14)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_B1	(_VSAPPBBX_BASE_ERROR_NUMBER - 15)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_C1	(_VSAPPBBX_BASE_ERROR_NUMBER - 16)
#define	_VSAPPBBX_UNK_XDTA_VERSION	(_VSAPPBBX_BASE_ERROR_NUMBER - 17)
#define	_VSAPPBBX_UNK_CHANNEL_XORDER	(_VSAPPBBX_BASE_ERROR_NUMBER - 18)
#define	_VSAPPBBX_UNK_CHANNEL_WEIGHT	(_VSAPPBBX_BASE_ERROR_NUMBER - 19)
#define	_VSAPPBBX_UNSUPPORTED_NVLS	(_VSAPPBBX_BASE_ERROR_NUMBER - 20)
#define	_VSAPPBBX_INIT_U_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER - 21)
#define	_VSAPPBBX_INIT_T_FAILURE	(_VSAPPBBX_BASE_ERROR_NUMBER - 22)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_A2	(_VSAPPBBX_BASE_ERROR_NUMBER - 23)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_B2	(_VSAPPBBX_BASE_ERROR_NUMBER - 24)
#define	_VSAPPBBX_RENDER_ERROR_TYPE_C2	(_VSAPPBBX_BASE_ERROR_NUMBER - 25)
#define	_VSAPPBBX_XT_HRTF_NULL		(_VSAPPBBX_BASE_ERROR_NUMBER - 26)

#define	_CHANNEL_XORDER_FUMA	0x0021
#define	_CHANNEL_XORDER_ACN	0x0022
#define	_CHANNEL_XORDER_SID	0x0023

#define	_CHANNEL_WEIGHT_FUMA	0x0041
#define	_CHANNEL_WEIGHT_SN3D	0x0042
#define	_CHANNEL_WEIGHT_N3D	0x0043

#define	_GPFIELD_ENABLE_TAIL	0x0001
#define	_GPFIELD_ENABLE_SFLT	0x0002

struct	_vsappbbx_xdta {
	short	_version;
	short	_chorder;
	short	_cweight;
	short	_pccount;
	int	_hrtfxln;
	int	_gpfield;
	double	*_mtht;
	double	*_mpsi;
};

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

	int _vsappbbx_get_version(int *_major_version, int *_minor_version, int *_release_date);
	int _vsappbbx_get_timestamp(char *_string_buffer);
	int _vsappbbx_init(void **_handle, int _pback_freq, int _block_size, int _channl_cnt, void *_hrtficb, void *_auxdata, const unsigned char *_licensekey);
	int _vsappbbx_vlsn(void **_handle, int *_vlsn_answer);
	int _vsappbbx_vlsd(void **_handle, double *_orientation_array, float **_data_inp, float **_data_out);
	int _vsappbbx_baud(void **_handle, double *_orientation_array, float **_data_inp, float **_data_out);
	int _vsappbbx_zout(void **_handle);
	int _vsappbbx_exit(void **_handle);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */


#endif /* __VSAPPBBX_H__ */