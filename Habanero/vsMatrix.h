//Habanero v1.0 (9-30-2016)

#ifndef __VSMATRIX_H__
#define __VSMATRIX_H__

#include <iostream>
#include <vector>

//SSE2
#include <pmmintrin.h>

#define EXVector(T)			std::vector<T>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
class Matrix {
public:
	Matrix();
	Matrix(int _rows, int _cols);
	Matrix(int _rows, int _cols, const T& _initial);
	Matrix(const EXVector(T) & rhs);
	~Matrix();

	void resize(int _rows, int _cols);
	void resize(int _rows, int _cols, const T& _initial);

	int getNumRows() const;
	int getNumCols() const;

	const EXVector(T)* getRowPtr(int _row) const;					//Return pointer to mat[_row]
	const EXVector(T) &  getRow(int _row) const;					//Return reference to mat[_row]
	bool setRow(int _row, const EXVector(T) & _row_data);			//Copy _row_data into mat[_row] if _row_data size is cols, and if (_row is in range) return true if successful, return false otherwise
	
	bool getCol(int _col, EXVector(T) & col_vector) const;			//Write column _col of mat into col_vector
	bool setCol(int _col, const  EXVector(T) & col_vector);			//Write col_vector into column _col of matrix


	bool freeMatrix();													//Clear contents of mat + deallocation

	//Operator overloading
	T & operator()(const int & row, const int & col);					//(0,0) retrieves entry at row and column
	const T & operator()(const int & row, const int & col) const;
	const T * mat_ptr(const int & row, const int & col);				//(0,0) retrieves const ptr at row and column

	Matrix<T> & operator = (const Matrix<T> & rhs);						//Moves RHS content to LHS

	//Matrix-scalar
	Matrix<T>  operator + (const T & rhs) const;
	Matrix<T>  operator - (const T & rhs) const;
	Matrix<T>  operator * (const T & rhs) const;
	Matrix<T>  operator / (const T & rhs) const;

	//Element-wise Matrix-matrix
	Matrix<T> operator+(const Matrix<T>& rhs) const;
	Matrix<T> operator-(const Matrix<T>& rhs) const;

	//Matrix-matrix
	Matrix<T> operator*(const Matrix<T>& rhs) const;					//Matrix-matrix mult
	Matrix<T> operator^(int p) const;									//Square-Matrix raised to power p
	bool operator==(const Matrix<T> & rhs) const;						//Equality, element by element
	bool equals(const Matrix<T> & rhs) const;							//Element by element comparison
	bool equals(const Matrix<T> & rhs, const T & tolerance) const;		//Element by element comparison with tolerance

	//Matrix-vector
	EXVector(T) operator*(const EXVector(T) & rhs) const;				//Matrix-vector product
	void MVP(const EXVector(T) & in, EXVector(T) & out) const;			//Matrix-vector product without reallocation
	void MVP_SSE2(const EXVector(float) & in, EXVector(float) & out) const;		//Matrix-vector product without reallocation with SSE2
	void MVP_SSE2(const EXVector(double) & in, EXVector(double) & out) const;		//Matrix-vector product without reallocation with SSE2

	//Special functions
	static Matrix<T> eye(int _rows, int _cols);							//Make a _row x _cols matrix with diagonal 1's alnog main diagonal, 0 everywhere else
	Matrix<T> transpose() const;										//Flip row, col, along with elements

	//Row-column access using matlab notations
	T & MAT_ij(const int & row, const int & col);
	const T & MAT_ij(const int & row, const int & col) const;

	void copyToArray(T * const out);									//Copy contents of mat to linear array out
	void copyToVector(EXVector(T) & out);								//Copy contents of mat to vector

	bool push_back_row(const EXVector(T) & in);							//Add row-vector in to end of mat
	void printToConsole() const;										//Print contents of mat 

protected:
	int rows;						//Number of rows
	int cols;						//Number of columns

	EXVector(EXVector(T)) mat;		//Outer vector rows, inner vector columns (row-major), i.e. mat[i][j] contains value at row i, column j
};

#endif