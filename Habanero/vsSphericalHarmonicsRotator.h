//Habanero v1.1 (10-28-2016)

#ifndef __VS_SPHERICAL_HARMONICS_ROTATOR__
#define __VS_SPHERICAL_HARMONICS_ROTATOR__

#include "vsMatrix.h"
#include "vsQuaternion.h"

////////////////////////////////////////////////// SPH and ACN are identical
//Channel order conversions
extern int VS_ACN2FUMA[16];		//i.e. channel n from ACN maps to channel VS_ACN2FUMA[n]
extern int VS_FUMA2ACN[16];
extern int VS_SPH2SPH[16];

//Different normalizations
//Coefficients given in SPH or ACN channel order
extern double FUMA_2_N3D_NORMAL[16];		//This is maxN to N3D
extern double SN3D_2_N3D_NORMAL[16];

#define MATRIX_TYPE Matrix

template <class T>
class VSSphericalHarmonicsRotator {
public:
	//Constructor, specify max order spherical harmonics for generating rotation matrices for
	VSSphericalHarmonicsRotator(int _max_sph_order = 0);
	
	//Call this whenever the sph order changes
	void init_max_sph_order(int _max_sph_order);	//Allocates sample buffers and rotation matrix R_order, initializes R_order's to identity
	int get_max_sph_order() const;

	//Rotation functions for a single sample vector of coordinates along sph bases. 
	//If in_out_stream is in b-format, the channel-order and normalizations must be converted to spherical harmonics (i.e. ACN and N3D). 
	//The output should then be converted back to the appropriate order and normalizations.
	void rotate_coefficients_inplace(EXVector(T) &  in_out_stream, const VSQuaternion<T> & q);
	void rotate_coefficients_inplace(EXVector(T) &  in_out_stream, const VSVector3<T> & euler_angles);		//Tait-Bryan intrinsic Z-Y'-X'' angles, euler_angles.x,y,z are roll, pitch, yaw angles
	void rotate_coefficients_inplace(EXVector(T) &  in_out_stream);											//Rotates using last computed R_order

	//Rotation functions for a row vector of colum vector samples (a matrix)
	void rotate_coefficients_inplace(Matrix<T> &  in_out_stream, const VSQuaternion<T> & q);
	void rotate_coefficients_inplace(Matrix<T> &  in_out_stream, const VSVector3<T> & euler_angles);		//Tait-Bryan intrinsic angles
	void rotate_coefficients_inplace(Matrix<T> &  in_out_stream, 
		const VSVector3<T> & euler_angles_start, const VSVector3<T> & euler_angles_end);					//Tait-Bryan intrinsic angles, SLERP rotation over in_out_stream
	void rotate_coefficients_inplace(Matrix<T> &  in_out_stream,
		const VSQuaternion<T> & q_start, const VSQuaternion<T> & q_end);									//SLERP rotation over in_out_stream
	void rotate_coefficients_inplace(Matrix<T> &  in_out_stream);											//Rotates block using last computed R_order

private:

	//Computes higher-order rotation matrices from first-order rotation matrix
	void compute_R_from_first_order_mtx(T R_mtx[3][3]);												
	void compute_R_from_quaternion_to_first_order_mtx(const VSQuaternion<T> & q);
	void compute_R_from_EulerZYXIntrinsic_to_first_order_mtx(const VSVector3<T> & euler_angles);

	void compute_R_higher();				//Compute higher-order spherical rotation matrices from the first order via recurrence relation
	void compute_R_higher_unrolled();		//Unrolled version of above
	void compute_R_higher_unrolled_2x();	//Unrolled corner cases of above
	void compute_R_higher_unrolled_2x_SSE2();	//SSE2 version of above
	void compute_R_higher_unrolled_3x();	//More unrolling

	void compute_UVW_table();

	inline T rot_P(int i, int l, int m, int n);
	inline T rot_W(int l, int m, int n);
	inline T rot_V(int l, int m, int n);
	inline T rot_U(int l, int m, int n);

	inline int order_2_num_basis(int order);

	int max_sph_order;
	int total_num_basis;

	EXVector(Matrix<T>) R_order;		//Rotation matrices for each order			
	EXVector(Matrix<T>) U_table, V_table, W_table;

	EXVector(T)			  sample_in_full;	//Full-length vector for storing input samples
	EXVector(EXVector(T)) sample_in_bufs;	//Buffers for storing samples for each order
	EXVector(EXVector(T)) sample_out_bufs;	//Buffers for storing samples for each order

};


#endif
