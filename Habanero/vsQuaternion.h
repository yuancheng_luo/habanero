//Habanero v1.0 (9-30-2016)

#ifndef __VS_QUATERNION__
#define __VS_QUATERNION__

#include <iostream>
#include "math.h"

#include "vsVector3.h"

///////////////////////////
template <class T>
class VSQuaternion {
public:
	T w;
	T x;
	T y;
	T z;

	//Constructors
	VSQuaternion();
	VSQuaternion(T _w, T _x, T _y, T _z);

	//Quaternion to Tait-Bryan Z-Y'-X'',  output v.z is yaw, v.y is pitch, v.x is roll 
	VSVector3<T> Quaternion2EulerZYXIntrinsic() const;

	VSQuaternion<T>		QuaternionNormalized() const;
	VSQuaternion<T>		QuaternionConj() const;

	inline T norm() const;

	//Tait-Bryan Z-Y'-X'' to Quaternion, euler_angle.x,y,z are roll, pitch, yaw angles in radians
	static VSQuaternion<T> EulerZYXIntrinsic2Quaternion(const VSVector3<T> & euler_angle);
	static T QuaternionInnerProduct(const VSQuaternion<T>  &   p, const VSQuaternion<T>  &   q);

	//Spherical linear interpolation between quaternions p, q at lambda fraction between [0, 1]
	static VSQuaternion<T>  QuaternionSLERP(const VSQuaternion<T>  &   p, const VSQuaternion<T>  &   q, T lambda);
	static VSVector3<T>	 EulerZYXIntrinsicSLERP(const VSVector3<T> & p, const VSVector3<T> & q, T lambda);

	//Rotate a vector via Hamilton product
	static VSVector3<T>	QuaternionVectorRotate(const VSQuaternion<T> & q, const VSVector3<T> & v);
	static VSQuaternion<T> QuaternionHamiltonProduct(const VSQuaternion<T> & u, const VSQuaternion<T> & v);

	bool equals(const VSQuaternion<T> &  q) const;
	bool equals(const VSQuaternion<T> &  q, T tol) const;

	void print() const;
	//Display human readable form, (angle, vector)
	void printAngleAxisForm() const;
};



#endif