#include "VSSphericalHarmonicsRotator.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Ivanic paper for recurrence relation between successive order rotation matrices http://pubs.acs.org/doi/pdf/10.1021/jp953350u
//https://github.com/polarch/Spherical-Harmonic-Transform/blob/master/getSHrotMtx.m
////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////// SPH and ACN channel order identical 
int VS_ACN2FUMA[16] =	{ 0, 2, 3, 1, 8, 6, 4, 5, 7, 15, 13, 11, 9, 10, 12, 14 };
int VS_FUMA2ACN[16] =	{ 0, 3, 1, 2, 6, 7, 5, 8, 4, 12, 13, 11, 14, 10, 15, 9 };
int VS_SPH2SPH[16] =	{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

double FUMA_2_N3D_NORMAL[16] = { sqrt(2.0), sqrt(3.0),sqrt(3.0), sqrt(3.0), sqrt(15.0) / 2, sqrt(15.0) / 2, sqrt(5.0), sqrt(15.0) / 2, sqrt(15.0) / 2, sqrt(35.0 / 8.0), sqrt(35.0) / 3, sqrt(224 / 45.0), sqrt(7.0), sqrt(224 / 45.0), sqrt(35.0) / 3, sqrt(35.0 / 8.0) };

double SN3D_2_N3D_NORMAL[16] = { 1, sqrt(3.0), sqrt(3.0), sqrt(3.0), sqrt(5.0), sqrt(5.0), sqrt(5.0), sqrt(5.0), sqrt(5.0), sqrt(7.0), sqrt(7.0), sqrt(7.0), sqrt(7.0), sqrt(7.0), sqrt(7.0), sqrt(7.0) };


////////////////////////////////////////////////////////////////////////////////
template <class T>
VSSphericalHarmonicsRotator<T>::VSSphericalHarmonicsRotator(int _max_sph_order)
{
	init_max_sph_order(_max_sph_order);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::init_max_sph_order(int _max_sph_order) {
	max_sph_order = _max_sph_order;

	int L = _max_sph_order + 1;
	total_num_basis = L*L;

	//Initialize sample buffers
	sample_in_full.resize(total_num_basis);
	sample_in_bufs.resize(L);
	sample_out_bufs.resize(L);

	//Initialize rotation matrices
	R_order.resize(L);
	for (int l = 0; l < L; ++l) {
		int num_basis = order_2_num_basis(l);
		//		R_order[l] = Matrix<T>(num_basis, num_basis, 0);
		R_order[l] = MATRIX_TYPE<T>::eye(num_basis, num_basis);			//Default to identity

		sample_in_bufs[l].resize(num_basis);
		sample_out_bufs[l].resize(num_basis);
	}

	//Compute and store UVW hash table
	compute_UVW_table();
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
int VSSphericalHarmonicsRotator<T>::get_max_sph_order() const{
	return max_sph_order;
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(EXVector(T) &  in_out_stream, const VSQuaternion<T> & q) {
	compute_R_from_quaternion_to_first_order_mtx(q);
	rotate_coefficients_inplace(in_out_stream);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(EXVector(T) &  in_out_stream, const VSVector3<T> & euler_angles) {
	compute_R_from_EulerZYXIntrinsic_to_first_order_mtx(euler_angles);
	rotate_coefficients_inplace(in_out_stream);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(EXVector(T) &  in_out_stream) {
	if (in_out_stream.size() == total_num_basis) {
		//Matrix-vec product of block-diagonal matrices
		for (int l = 0; l <= max_sph_order; ++l) {
			int num_basis = order_2_num_basis(l);
			memcpy(&sample_in_bufs[l][0], &in_out_stream[l*l], sizeof(T) * num_basis);
	//		sample_out_bufs[l] = R_order[l] * sample_in_bufs[l];		//version one with reallocation
			//R_order[l].MVP(sample_in_bufs[l], sample_out_bufs[l]);	//version two w/o reallocation
			R_order[l].MVP_SSE2(sample_in_bufs[l], sample_out_bufs[l]);	//SSE2 version
			memcpy(&in_out_stream[l*l], &sample_out_bufs[l][0], sizeof(T) * num_basis);
		}

	}else
		fprintf(stderr, "in_out_stream size != %d\n", total_num_basis);
}


////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(Matrix<T> &  in_out_stream, const VSQuaternion<T> & q) {
	compute_R_from_quaternion_to_first_order_mtx(q);
	rotate_coefficients_inplace(in_out_stream);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(Matrix<T> &  in_out_stream, const VSVector3<T> & euler_angles) {
	compute_R_from_EulerZYXIntrinsic_to_first_order_mtx(euler_angles);
	rotate_coefficients_inplace(in_out_stream);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(Matrix<T> &  in_out_stream,
	const VSQuaternion<T> & q_start, const VSQuaternion<T> & q_end) {

	int num_channels = in_out_stream.getNumRows();
	if (num_channels == total_num_basis) {

		//Iterate over samples 
		int num_samples = in_out_stream.getNumCols();
		for (int i = 0; i < num_samples; ++i) {
			in_out_stream.getCol(i, sample_in_full);						//Grab column data from matrix

			//Compute SLERP'd Quaternion
			T lambda = ((T)i) / ((T)num_samples);
			VSQuaternion<T> q_SLERP = VSQuaternion<T>::QuaternionSLERP(q_start, q_end, lambda);
			
			//Perform rotation
			rotate_coefficients_inplace(sample_in_full, q_SLERP);
			in_out_stream.setCol(i, sample_in_full);			//Copy back into matrix
		}
	}
	else
		fprintf(stderr, "in_out_stream rows != %d\n", total_num_basis);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(Matrix<T> &  in_out_stream,
	const VSVector3<T> & euler_angles_start, const VSVector3<T> & euler_angles_end) {

	VSQuaternion<T> q_start = VSQuaternion<T>::EulerZYXIntrinsic2Quaternion(euler_angles_start);
	VSQuaternion<T> q_end = VSQuaternion<T>::EulerZYXIntrinsic2Quaternion(euler_angles_end);
	rotate_coefficients_inplace(in_out_stream, q_start, q_end);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::rotate_coefficients_inplace(Matrix<T> &  in_out_stream) {
	if (in_out_stream.getNumRows() == total_num_basis) {
		//Iterate over samples 
		int num_samples = in_out_stream.getNumCols();
		for (int i = 0; i < num_samples; ++i) {
			in_out_stream.getCol(i, sample_in_full);						//Grab column data from matrix

			//Perform rotation
			rotate_coefficients_inplace(sample_in_full);
			in_out_stream.setCol(i, sample_in_full);			//Copy back into matrix
		}

	}else
		fprintf(stderr, "in_out_stream rows != %d\n", total_num_basis);
}

////////////////////////////////////////////////////////////////////////////////
//Input: First order rotation matrix R_mtx
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_from_first_order_mtx(T R_mtx[3][3]){

	if (max_sph_order > 0) {
		//Fill first-order matrix
		/*R_order[1].MAT_ij(1, 1) = R_mtx[0][0];
		R_order[1].MAT_ij(1, 2) = R_mtx[0][1];
		R_order[1].MAT_ij(1, 3) = R_mtx[0][2];

		R_order[1].MAT_ij(2, 1) = R_mtx[1][0];
		R_order[1].MAT_ij(2, 2) = R_mtx[1][1];
		R_order[1].MAT_ij(2, 3) = R_mtx[1][2];

		R_order[1].MAT_ij(3, 1) = R_mtx[2][0];
		R_order[1].MAT_ij(3, 2) = R_mtx[2][1];
		R_order[1].MAT_ij(3, 3) = R_mtx[2][2];*/

		//Reorder channels 
		R_order[1].MAT_ij(1, 1) = R_mtx[1][1];	//Y
		R_order[1].MAT_ij(1, 2) = R_mtx[1][2];
		R_order[1].MAT_ij(1, 3) = R_mtx[1][0];

		R_order[1].MAT_ij(2, 1) = R_mtx[2][1];	//Z
		R_order[1].MAT_ij(2, 2) = R_mtx[2][2];
		R_order[1].MAT_ij(2, 3) = R_mtx[2][0];

		R_order[1].MAT_ij(3, 1) = R_mtx[0][1];	//X
		R_order[1].MAT_ij(3, 2) = R_mtx[0][2];
		R_order[1].MAT_ij(3, 3) = R_mtx[0][0];
	
		//compute_R_higher();
		//compute_R_higher_unrolled();	//Macro version 2.5x faster
	//	compute_R_higher_unrolled_2x();	//Some more unrolling of corner cases
		compute_R_higher_unrolled_2x_SSE2();	//SSE2 version
		//compute_R_higher_unrolled_3x();	//Too much, not enough register space
	}
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_from_quaternion_to_first_order_mtx(const VSQuaternion<T> &  q) {
	VSVector3<T> v = q.Quaternion2EulerZYXIntrinsic();
	compute_R_from_EulerZYXIntrinsic_to_first_order_mtx(v);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_from_EulerZYXIntrinsic_to_first_order_mtx(const VSVector3<T> & euler_angles) {
	T R_mtx[3][3];
	euler_angles.EulerZYXIntrinsic2Matrix(R_mtx);
	compute_R_from_first_order_mtx(R_mtx);
}

////////////////////////////////////////////////////////////////////////////////
template <class T>
inline int VSSphericalHarmonicsRotator<T>::order_2_num_basis(int order) {
	return 2 * order + 1; //-order to +order
}

/////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_UVW_table() {
	if (max_sph_order > 0) {
		U_table.resize(max_sph_order + 1);
		V_table.resize(max_sph_order + 1);
		W_table.resize(max_sph_order + 1);

		//compute UVW hash table for each order
		for (int l = 2; l <= max_sph_order; ++l) {
			int num_basis = order_2_num_basis(l);
			U_table[l] = MATRIX_TYPE<T>(num_basis, num_basis);
			V_table[l] = MATRIX_TYPE<T>(num_basis, num_basis);
			W_table[l] = MATRIX_TYPE<T>(num_basis, num_basis);

			for (int m = -l; m <= l; ++m) {
				for (int n = -l; n <= l; ++n) {
					// compute u, v, w terms of Eq.8.1 (Table I)
					int d = (m == 0); // the delta function d_m0
					T denom = (abs(n) == l ? (2 * l)*(2 * l - 1) : (l*l - n*n));
					U_table[l](m + l, n + l) = sqrt(((T)(l*l - m*m)) / denom );
					V_table[l](m + l, n + l) = sqrt((T)(1 + d)*(l + abs(m) - 1)*(l + abs(m)) / denom)*(1 - 2 * d)*0.5;
					W_table[l](m + l, n + l) = sqrt((T)(l - abs(m) - 1)*(l - abs(m)) / denom) * (1 - d) * (-0.5);
				}
			}
		}
	}
}


/////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_higher() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		for (int l = 2; l <= max_sph_order; ++l) {
			for (int m = -l; m <= l; ++m) {
				for (int n = -l; n <= l; ++n) {
			
					//// compute u, v, w terms of Eq.8.1 (Table I)
					//int d = (m == 0); // the delta function d_m0
					//T denom = (abs(n) == l ? (2 * l)*(2 * l - 1) : (l*l - n*n));
					//T u = sqrt((T)(l*l - m*m) / denom);
					//T v = sqrt((T)(1 + d)*(l + abs(m) - 1)*(l + abs(m)) / denom)*(1 - 2 * d)*0.5;
					//T w = sqrt((T)(l - abs(m) - 1)*(l - abs(m)) / denom) * (1 - d) * (-0.5);

					//// computes Eq.8.1
					//u = (u != 0 ? u*rot_U(l, m, n) : u);
					//v = (v != 0 ? v*rot_V(l, m, n) : v);
					//w = (w != 0 ? w*rot_W(l, m, n) : w);

					//Hash table version of above
					T u = U_table[l](m + l, n + l);
					T v = V_table[l](m + l, n + l);
					T w = W_table[l](m + l, n + l);
					if (u != 0)
						u *= rot_U(l, m, n);
					if (v != 0)
						v *= rot_V(l, m, n);
					if (w != 0)
						w *= rot_W(l, m, n);

					R_order[l].MAT_ij(m + l + 1, n + l + 1) = u + v + w;
				}
			}
		}
	}

}

#define ROT_P_MACRO(i, l, m, n) ((n) == -(l) ?  R_order[1].MAT_ij((i) + 2, 3) * R_order[(l) - 1].MAT_ij((m) + (l), 1) + R_order[1].MAT_ij((i) + 2, 1) * R_order[(l) - 1].MAT_ij((m) + (l), 2 * (l) - 1)  : ((n) == (l) ? (R_order[1].MAT_ij((i) + 2, 3) * R_order[(l) - 1].MAT_ij((m) + (l), 2 * (l) - 1) - R_order[1].MAT_ij((i) + 2, 1) * R_order[(l) - 1].MAT_ij((m) + (l), 1)) : R_order[1].MAT_ij((i) + 2, 2) * R_order[(l) - 1].MAT_ij((m) + (l), (n) + (l))))

/////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_higher_unrolled() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		for (int l = 2; l <= max_sph_order; ++l) {
			for (int m = -l; m <= l; ++m) {
				for (int n = -l; n <= l; ++n) {
					T u = U_table[l](m + l, n + l);
					T v = V_table[l](m + l, n + l);
					T w = W_table[l](m + l, n + l);

					if (u != 0)
						u *= ROT_P_MACRO(0, l, m, n);

					if (v != 0) {
						if (m == 0) 
							v *= ROT_P_MACRO(1, l, 1, n) + ROT_P_MACRO(-1, l, -1, n);
						else {
							if (m > 0) {
								if (m == 1) 
									v *= ROT_P_MACRO(1, l, m - 1, n) * VS_SQRT_2;
								else
									v *= ROT_P_MACRO(1, l, m - 1, n) - ROT_P_MACRO(-1, l, -m + 1, n);
							}
							else {
								if (m == -1)
									v *= ROT_P_MACRO(-1, l, -m - 1, n)  * VS_SQRT_2;
								else
									v *= ROT_P_MACRO(1, l, m + 1, n) + ROT_P_MACRO(-1, l, -m - 1, n);
							}
						}
					}

					if (w != 0) {
						if (m > 0)
							w *= ROT_P_MACRO(1, l, m + 1, n) + ROT_P_MACRO(-1, l, -m - 1, n);
						else
							w *= ROT_P_MACRO(1, l, m - 1, n) - ROT_P_MACRO(-1, l, -m + 1, n);
					}

					R_order[l].MAT_ij(m + l + 1, n + l + 1) = u + v + w;
				}
			}

		}
	}
}


#define ROT_P_MACRO_N_IS_NEGL(i, l, m)		(R_order[1]((i) + 1, 2) * R_order[(l) - 1]((m) + (l) - 1, 0) + R_order[1]((i) + 1, 0) * R_order[(l) - 1]((m) + (l) - 1, 2 * ((l) - 1)))
#define ROT_P_MACRO_N_IS_L(i, l, m)			(R_order[1]((i) + 1, 2) * R_order[(l) - 1]((m) + (l) - 1, 2 * ((l)-1) ) - R_order[1]((i) + 1, 0) * R_order[(l) - 1]((m) + (l) - 1, 0))
#define ROT_P_MACRO_N_ELSE(i, l, m, n)		(R_order[1]((i) + 1, 1) * R_order[(l) - 1]((m) + (l) - 1, (n) + (l) - 1))

/////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_higher_unrolled_2x() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		for (int l = 2; l <= max_sph_order; ++l) {
			for (int m = -l; m <= l; ++m) {
				//Boundary case: n = -l
				T u = U_table[l](m + l, 0);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, m);
				T v = V_table[l](m + l, 0);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_NEGL(1, l, 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m + 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1));
					}
				}
				T w = W_table[l](m + l, 0);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_NEGL(1, l, m + 1, -l) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1, -l) : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1, -l) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1, -l));
				R_order[l](m + l, 0) = u + v + w;

				//Central entries
				if (m < -1) {
					for (int n = -l + 1; n < l; ++n) {
						T u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						T v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						T w = W_table[l](m + l, n + l);
						if (w != 0)	w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
				}
				else if (m > 1) {
					for (int n = -l + 1; n < l; ++n) {
						T u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						T v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						T w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
				}
				else if (m == -1) {
					for (int n = -l + 1; n < l; ++n) {
						T u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						T v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n)  * VS_SQRT_2;
						T w = W_table[l](m + l, n + l);
						if (w != 0)	w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
				}
				else if (m == 1) {
					for (int n = -l + 1; n < l; ++n) {
						T u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						T v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) * VS_SQRT_2;
						T w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
				}
				else { //m == 0
					for (int n = -l + 1; n < l; ++n) {
						T u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						T v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -1, n);
						T w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
				}

				/*	for (int n = -l + 1; n < l; ++n {
				T u = U_table[l](m + l, n + l);
				if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
				T v = V_table[l](m + l, n + l);
				if (v != 0) {
				if (m == 0)	v *= ROT_P_MACRO_N_ELSE(1, l, 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -1, n);
				else {
				if (m > 0)	v *= (m > 1 ? ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n) : ROT_P_MACRO_N_ELSE(1, l, m - 1, n) * VS_SQRT_2);
				else		v *= (m < -1 ? ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n) : ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n)  * VS_SQRT_2);
				}
				}
				T w = W_table[l](m + l, n + l);
				if (w != 0)	w *= (m > 0 ? ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n) : ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n));
				R_order[l].MAT_ij(m + l + 1, n + l + 1) = u + v + w;
				}*/

				//Boundary case: n = l
				u = U_table[l](m + l, 2*l);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, m);
				v = V_table[l](m + l, 2 * l);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_L(1, l, 1) + ROT_P_MACRO_N_IS_L(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_L(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_L(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1));
					}
				}
				w = W_table[l](m + l, 2 * l);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1) : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
				R_order[l](m + l, 2*l) = u + v + w;
			}
			////R_order[l].printToConsole();
			//U_table[l].printToConsole();
			//V_table[l].printToConsole();
			//W_table[l].printToConsole();
			//printf("max_sph_order = %d l = %d\n", max_sph_order, l);
			//getchar();
		}
	}
}


#define ROT_P_MACRO_N_ELSE_SSE2(l, m, n)		(((m) + (l) >= 1) && ((m) <= (l) - 1 )  ? _mm_loadu_ps(R_order[(l) - 1].mat_ptr((m) + (l) - 1, (n) + (l) - 1)) : _mm_set1_ps(0))
/////////////////////////////////////////////////////
void VSSphericalHarmonicsRotator<float>::compute_R_higher_unrolled_2x_SSE2() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		//Load first-order relevant rotation entries
		__m128 SSE_R1_0 = _mm_set1_ps(R_order[1](0, 1));
		__m128 SSE_R1_1 = _mm_set1_ps(R_order[1](1, 1));
		__m128 SSE_R1_2 = _mm_set1_ps(R_order[1](2, 1));

		for (int l = 2; l <= max_sph_order; ++l) {
			int num_n_central_mod4 = (2 * l - 1) & 0x3;
			int n_start = -l + 1 + num_n_central_mod4;

			for (int m = -l; m <= l; ++m) {
				//Boundary case: n = -l
				float u = U_table[l](m + l, 0);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, m);
				float v = V_table[l](m + l, 0);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_NEGL(1, l, 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m + 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1));
					}
				}
				float w = W_table[l](m + l, 0);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_NEGL(1, l, m + 1, -l) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1, -l) : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1, -l) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1, -l));
				R_order[l].MAT_ij(m + l + 1, 1) = u + v + w;

				//Central entries
				__m128 SSE_U, SSE_V, SSE_W;
				int n = -l + 1;
				if (m < -1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 4) {
						SSE_U = _mm_loadu_ps(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_ps(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_ps(&W_table[l](m + l, n + l));

						__m128 SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2(l, m, n);
						__m128 SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2(l, m + 1, n);
						__m128 SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m - 1, n);
						__m128 SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2(l, m - 1, n);
						__m128 SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m + 1, n);

						SSE_U = _mm_mul_ps(SSE_U, _mm_mul_ps(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_ps(SSE_V, _mm_add_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_v1), _mm_mul_ps(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_ps(SSE_W, _mm_sub_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_w1), _mm_mul_ps(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_ps(&R_order[l](m + l, n + l), _mm_add_ps(_mm_add_ps(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m > 1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 4) {
						SSE_U = _mm_loadu_ps(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_ps(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_ps(&W_table[l](m + l, n + l));

						__m128 SSE_Rl_u =  ROT_P_MACRO_N_ELSE_SSE2(l, m, n);
						__m128 SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2(l, m - 1, n);
						__m128 SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m + 1, n);
						__m128 SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2(l, m + 1, n);
						__m128 SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m - 1, n);

						SSE_U = _mm_mul_ps(SSE_U, _mm_mul_ps(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_ps(SSE_V, _mm_sub_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_v1), _mm_mul_ps(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_ps(SSE_W, _mm_add_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_w1), _mm_mul_ps(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_ps(&R_order[l](m + l, n + l), _mm_add_ps(_mm_add_ps(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m == -1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n)  * VS_SQRT_2;
						w = W_table[l](m + l, n + l);
						if (w != 0)	w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 4) {
						SSE_U = _mm_loadu_ps(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_ps(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_ps(&W_table[l](m + l, n + l));

						__m128 SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2(l, m, n);
						__m128 SSE_Rl_v = ROT_P_MACRO_N_ELSE_SSE2(l, -m - 1, n);
						__m128 SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2(l, m - 1, n);
						__m128 SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m + 1, n);

						SSE_U = _mm_mul_ps(SSE_U, _mm_mul_ps(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_ps(SSE_V, _mm_mul_ps(_mm_mul_ps(SSE_R1_0, SSE_Rl_v), _mm_set1_ps(VS_SQRT_2)));
						SSE_W = _mm_mul_ps(SSE_W, _mm_sub_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_w1), _mm_mul_ps(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_ps(&R_order[l](m + l, n + l), _mm_add_ps(_mm_add_ps(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m == 1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) * VS_SQRT_2;
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 4) {
						SSE_U = _mm_loadu_ps(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_ps(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_ps(&W_table[l](m + l, n + l));

						__m128 SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2(l, m, n);
						__m128 SSE_Rl_v = ROT_P_MACRO_N_ELSE_SSE2(l, m - 1, n);
						__m128 SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2(l, m + 1, n);
						__m128 SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m - 1, n);

						SSE_U = _mm_mul_ps(SSE_U, _mm_mul_ps(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_ps(SSE_V, _mm_mul_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_v), _mm_set1_ps(VS_SQRT_2)));
						SSE_W = _mm_mul_ps(SSE_W, _mm_add_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_w1), _mm_mul_ps(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_ps(&R_order[l](m + l, n + l), _mm_add_ps(_mm_add_ps(SSE_U, SSE_V), SSE_W));
					}
				}
				else { //m == 0
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 4) {
						SSE_U = _mm_loadu_ps(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_ps(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_ps(&W_table[l](m + l, n + l));

						__m128 SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2(l, m, n);
						__m128 SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2(l, 1, n);
						__m128 SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2(l, -1, n);
						__m128 SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2(l, m - 1, n);
						__m128 SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2(l, -m + 1, n);

						SSE_U = _mm_mul_ps(SSE_U, _mm_mul_ps(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_ps(SSE_V, _mm_add_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_v1), _mm_mul_ps(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_ps(SSE_W, _mm_sub_ps(_mm_mul_ps(SSE_R1_2, SSE_Rl_w1), _mm_mul_ps(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_ps(&R_order[l](m + l, n + l), _mm_add_ps(_mm_add_ps(SSE_U, SSE_V), SSE_W));
					}
				}

				//Boundary case: n = l
				u = U_table[l](m + l, 2 * l);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, m);
				v = V_table[l](m + l, 2 * l);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_L(1, l, 1) + ROT_P_MACRO_N_IS_L(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_L(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_L(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1));
					}
				}
				w = W_table[l](m + l, 2 * l);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1) : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
				R_order[l](m + l, 2 * l) = u + v + w;
			}
		}
	}
}

#define ROT_P_MACRO_N_ELSE_SSE2d(l, m, n)		(((m) + (l) >= 1) && ((m) <= (l) - 1 )  ? _mm_loadu_pd(R_order[(l) - 1].mat_ptr((m) + (l) - 1, (n) + (l) - 1)) : _mm_set1_pd(0))

/////////////////////////////////////////////////////
void VSSphericalHarmonicsRotator<double>::compute_R_higher_unrolled_2x_SSE2() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		//Load first-order relevant rotation entries
		__m128d SSE_R1_0 = _mm_set1_pd(R_order[1](0, 1));
		__m128d SSE_R1_1 = _mm_set1_pd(R_order[1](1, 1));
		__m128d SSE_R1_2 = _mm_set1_pd(R_order[1](2, 1));

		for (int l = 2; l <= max_sph_order; ++l) {
			int num_n_central_mod2 = (2 * l - 1) & 0x1;
			int n_start = -l + 1 + num_n_central_mod2;

			for (int m = -l; m <= l; ++m) {
				//Boundary case: n = -l
				double u = U_table[l](m + l, 0);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, m);
				double v = V_table[l](m + l, 0);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_NEGL(1, l, 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m + 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1));
					}
				}
				double w = W_table[l](m + l, 0);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_NEGL(1, l, m + 1, -l) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1, -l) : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1, -l) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1, -l));
				R_order[l].MAT_ij(m + l + 1, 1) = u + v + w;

				//Central entries
				__m128d SSE_U, SSE_V, SSE_W;
				int n = -l + 1;
				if (m < -1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 2) {
						SSE_U = _mm_loadu_pd(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_pd(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_pd(&W_table[l](m + l, n + l));

						__m128d SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2d(l, m, n);
						__m128d SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m + 1, n);
						__m128d SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m - 1, n);
						__m128d SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m - 1, n);
						__m128d SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m + 1, n);

						SSE_U = _mm_mul_pd(SSE_U, _mm_mul_pd(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_pd(SSE_V, _mm_add_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_v1), _mm_mul_pd(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_pd(SSE_W, _mm_sub_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_w1), _mm_mul_pd(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_pd(&R_order[l](m + l, n + l), _mm_add_pd(_mm_add_pd(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m > 1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 2) {
						SSE_U = _mm_loadu_pd(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_pd(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_pd(&W_table[l](m + l, n + l));

						__m128d SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2d(l, m, n);
						__m128d SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m - 1, n);
						__m128d SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m + 1, n);
						__m128d SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m + 1, n);
						__m128d SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m - 1, n);

						SSE_U = _mm_mul_pd(SSE_U, _mm_mul_pd(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_pd(SSE_V, _mm_sub_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_v1), _mm_mul_pd(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_pd(SSE_W, _mm_add_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_w1), _mm_mul_pd(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_pd(&R_order[l](m + l, n + l), _mm_add_pd(_mm_add_pd(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m == -1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0) 	v *= ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n)  * VS_SQRT_2;
						w = W_table[l](m + l, n + l);
						if (w != 0)	w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 2) {
						SSE_U = _mm_loadu_pd(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_pd(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_pd(&W_table[l](m + l, n + l));

						__m128d SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2d(l, m, n);
						__m128d SSE_Rl_v = ROT_P_MACRO_N_ELSE_SSE2d(l, -m - 1, n);
						__m128d SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m - 1, n);
						__m128d SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m + 1, n);

						SSE_U = _mm_mul_pd(SSE_U, _mm_mul_pd(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_pd(SSE_V, _mm_mul_pd(_mm_mul_pd(SSE_R1_0, SSE_Rl_v), _mm_set1_pd(VS_SQRT_2)));
						SSE_W = _mm_mul_pd(SSE_W, _mm_sub_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_w1), _mm_mul_pd(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_pd(&R_order[l](m + l, n + l), _mm_add_pd(_mm_add_pd(SSE_U, SSE_V), SSE_W));
					}
				}
				else if (m == 1) {
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) * VS_SQRT_2;
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 2) {
						SSE_U = _mm_loadu_pd(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_pd(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_pd(&W_table[l](m + l, n + l));

						__m128d SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2d(l, m, n);
						__m128d SSE_Rl_v = ROT_P_MACRO_N_ELSE_SSE2d(l, m - 1, n);
						__m128d SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m + 1, n);
						__m128d SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m - 1, n);

						SSE_U = _mm_mul_pd(SSE_U, _mm_mul_pd(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_pd(SSE_V, _mm_mul_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_v), _mm_set1_pd(VS_SQRT_2)));
						SSE_W = _mm_mul_pd(SSE_W, _mm_add_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_w1), _mm_mul_pd(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_pd(&R_order[l](m + l, n + l), _mm_add_pd(_mm_add_pd(SSE_U, SSE_V), SSE_W));
					}
				}
				else { //m == 0
					for (; n < n_start; ++n) {
						u = U_table[l](m + l, n + l);
						if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
						v = V_table[l](m + l, n + l);
						if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -1, n);
						w = W_table[l](m + l, n + l);
						if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
						R_order[l](m + l, n + l) = u + v + w;
					}
					for (; n < l; n += 2) {
						SSE_U = _mm_loadu_pd(&U_table[l](m + l, n + l));
						SSE_V = _mm_loadu_pd(&V_table[l](m + l, n + l));
						SSE_W = _mm_loadu_pd(&W_table[l](m + l, n + l));

						__m128d SSE_Rl_u = ROT_P_MACRO_N_ELSE_SSE2d(l, m, n);
						__m128d SSE_Rl_v1 = ROT_P_MACRO_N_ELSE_SSE2d(l, 1, n);
						__m128d SSE_Rl_v2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -1, n);
						__m128d SSE_Rl_w1 = ROT_P_MACRO_N_ELSE_SSE2d(l, m - 1, n);
						__m128d SSE_Rl_w2 = ROT_P_MACRO_N_ELSE_SSE2d(l, -m + 1, n);

						SSE_U = _mm_mul_pd(SSE_U, _mm_mul_pd(SSE_R1_1, SSE_Rl_u));
						SSE_V = _mm_mul_pd(SSE_V, _mm_add_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_v1), _mm_mul_pd(SSE_R1_0, SSE_Rl_v2)));
						SSE_W = _mm_mul_pd(SSE_W, _mm_sub_pd(_mm_mul_pd(SSE_R1_2, SSE_Rl_w1), _mm_mul_pd(SSE_R1_0, SSE_Rl_w2)));

						_mm_storeu_pd(&R_order[l](m + l, n + l), _mm_add_pd(_mm_add_pd(SSE_U, SSE_V), SSE_W));
					}
				}

				//Boundary case: n = l
				u = U_table[l](m + l, 2 * l);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, m);
				v = V_table[l](m + l, 2 * l);
				if (v != 0) {
					if (m == 0)	v *= ROT_P_MACRO_N_IS_L(1, l, 1) + ROT_P_MACRO_N_IS_L(-1, l, -1);
					else {
						if (m > 0)	v *= (m == 1 ? ROT_P_MACRO_N_IS_L(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
						else		v *= (m == -1 ? ROT_P_MACRO_N_IS_L(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1));
					}
				}
				w = W_table[l](m + l, 2 * l);
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1) : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
				R_order[l](m + l, 2 * l) = u + v + w;
			}
		}
	}
}


/////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsRotator<T>::compute_R_higher_unrolled_3x() {

	if (max_sph_order > 0) {
		//compute rotation matrix of each order recursively
		for (int l = 2; l <= max_sph_order; ++l) {

			//////////////////Case m < 0
			for (int m = -l; m < 0; ++m) {
				//Boundary case: n = -l
				T u = U_table[l](m + l, 0);	T v = V_table[l](m + l, 0);	T w = W_table[l](m + l, 0);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, m);
				if (v != 0)		v *= (m == -1 ? ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m + 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1));
				if (w != 0)		w *= (m > 0 ? ROT_P_MACRO_N_IS_NEGL(1, l, m + 1, -l) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1, -l) : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1, -l) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1, -l));
				R_order[l].MAT_ij(m + l + 1, 1) = u + v + w;
				//Central entries
				for (int n = -l + 1; n < l; ++n) {
					T u = U_table[l](m + l, n + l);	T v = V_table[l](m + l, n + l);	T w = W_table[l](m + l, n + l);
					if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
					if (v != 0)		v *= (m == -1 ? ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n)  * VS_SQRT_2 : ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n));
					if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n);
					R_order[l].MAT_ij(m + l + 1, n + l + 1) = u + v + w;
				}
				//Boundary case: n = l
				u = U_table[l](m + l, 2 * l);	v = V_table[l](m + l, 2 * l);	w = W_table[l](m + l, 2 * l);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, m);
				if (v != 0)		v *= (m == -1 ? ROT_P_MACRO_N_IS_L(-1, l, -m - 1)  * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1));
				if (w != 0)		w *= ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1);
				R_order[l].MAT_ij(m + l + 1, 2 * l + 1) = u + v + w;
			}

			//////////////////Case m = 0
			//Boundary case: n = -l
			T u = U_table[l](l, 0);	T v = V_table[l](l, 0);	T w = W_table[l](l, 0);
			if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, 0);
			if (v != 0)		v *= ROT_P_MACRO_N_IS_NEGL(1, l, 1) + ROT_P_MACRO_N_IS_NEGL(-1, l, -1);
			if (w != 0)		w *= ROT_P_MACRO_N_IS_NEGL(1, l, -1, -l) - ROT_P_MACRO_N_IS_NEGL(-1, l, 1, -l);
			R_order[l].MAT_ij(l + 1, 1) = u + v + w;
			//Central entries
			for (int n = -l + 1; n < l; ++n) {
				T u = U_table[l](l, n + l);	T v = V_table[l](l, n + l);	T w = W_table[l](l, n + l);
				if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, 0, n);
				if (v != 0)		v *= ROT_P_MACRO_N_ELSE(1, l, 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -1, n);
				if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, -1, n) - ROT_P_MACRO_N_ELSE(-1, l, 1, n);
				R_order[l].MAT_ij(l + 1, n + l + 1) = u + v + w;
			}
			//Boundary case: n = l
			u = U_table[l](l, 2 * l);	v = V_table[l](l, 2 * l);	w = W_table[l](l, 2 * l);
			if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, 0);
			if (v != 0)		v *= ROT_P_MACRO_N_IS_L(1, l, 1) + ROT_P_MACRO_N_IS_L(-1, l, -1);
			if (w != 0)		w *= ROT_P_MACRO_N_IS_L(1, l, -1) - ROT_P_MACRO_N_IS_L(-1, l, 1);
			R_order[l].MAT_ij(l + 1, 2 * l + 1) = u + v + w;

			//////////////////Case m > 0
			for (int m = 1; m <= l; ++m) {
				//Boundary case: n = -l
				T u = U_table[l](m + l, 0);	T v = V_table[l](m + l, 0);	T w = W_table[l](m + l, 0);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_NEGL(0, l, m);
				if (v != 0)		v *= (m == 1 ? ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_NEGL(1, l, m - 1) - ROT_P_MACRO_N_IS_NEGL(-1, l, -m + 1));
				if (w != 0)		w *= ROT_P_MACRO_N_IS_NEGL(1, l, m + 1, -l) + ROT_P_MACRO_N_IS_NEGL(-1, l, -m - 1, -l);
				R_order[l].MAT_ij(m + l + 1, 1) = u + v + w;
				//Central entries
				for (int n = -l + 1; n < l; ++n) {
					T u = U_table[l](m + l, n + l);	T v = V_table[l](m + l, n + l);	T w = W_table[l](m + l, n + l);
					if (u != 0)		u *= ROT_P_MACRO_N_ELSE(0, l, m, n);
					if (v != 0) 	v *= (m == 1 ? ROT_P_MACRO_N_ELSE(1, l, m - 1, n) * VS_SQRT_2 : ROT_P_MACRO_N_ELSE(1, l, m - 1, n) - ROT_P_MACRO_N_ELSE(-1, l, -m + 1, n));
					if (w != 0)		w *= ROT_P_MACRO_N_ELSE(1, l, m + 1, n) + ROT_P_MACRO_N_ELSE(-1, l, -m - 1, n);
					R_order[l].MAT_ij(m + l + 1, n + l + 1) = u + v + w;
				}
				//Boundary case: n = l
				u = U_table[l](m + l, 2 * l);	v = V_table[l](m + l, 2 * l);	w = W_table[l](m + l, 2 * l);
				if (u != 0)		u *= ROT_P_MACRO_N_IS_L(0, l, m);
				if (v != 0)		v *= (m == 1 ? ROT_P_MACRO_N_IS_L(1, l, m - 1) * VS_SQRT_2 : ROT_P_MACRO_N_IS_L(1, l, m - 1) - ROT_P_MACRO_N_IS_L(-1, l, -m + 1));
				if (w != 0)		w *= ROT_P_MACRO_N_IS_L(1, l, m + 1) + ROT_P_MACRO_N_IS_L(-1, l, -m - 1);
				R_order[l].MAT_ij(m + l + 1, 2 * l + 1) = u + v + w;
			}

		}
	}
}




//% function to compute term P of U,V,W (Table II)
//function [ret] = P(i,l,a,b,R_1,R_lm1)
//
//ri1 = R_1(i+2,1+2);
//rim1 = R_1(i+2,-1+2);
//ri0 = R_1(i+2,0+2);
//
//if (b==-l)
//    ret = ri1*R_lm1(a+l,1) + rim1*R_lm1(a+l, 2*l-1);
//else
//    if (b==l)
//        ret = ri1*R_lm1(a+l,2*l-1) - rim1*R_lm1(a+l, 1);        
//    else
//        ret = ri0*R_lm1(a+l,b+l);
//    end
//end
//
//end
template <class T>
inline T VSSphericalHarmonicsRotator<T>::rot_P(int i, int l, int m, int n) {
	//T ri1 = R_order[1].MAT_ij(i + 2, 1 + 2);
	//T rim1 = R_order[1].MAT_ij(i + 2, -1 + 2);
	//T ri0 = R_order[1].MAT_ij(i + 2, 0 + 2);

	T ri1 = R_order[1].MAT_ij(i + 2, 3);
	T rim1 = R_order[1].MAT_ij(i + 2, 1);
	T ri0 = R_order[1].MAT_ij(i + 2, 2);


	if (n == -l)
	    return ri1 * R_order[l - 1].MAT_ij(m+l, 1)	+	rim1 * R_order[l - 1].MAT_ij(m+l, 2*l-1);
	else {
		if (n == l)
			return ri1 * R_order[l - 1].MAT_ij(m + l, 2 * l - 1) - rim1 * R_order[l - 1].MAT_ij(m + l, 1);
		else
			return ri0 * R_order[l - 1].MAT_ij(m + l, n + l);
	}
}


//function [ret] = W(l,m,n,R_1,R_lm1)
//
//if (m==0)
//    error('should not be called')
//else
//    if (m>0)
//        p0 = P(1,l,m+1,n,R_1,R_lm1);
//        p1 = P(-1,l,-m-1,n,R_1,R_lm1);        
//        ret = p0 + p1;
//    else
//        p0 = P(1,l,m-1,n,R_1,R_lm1);
//        p1 = P(-1,l,-m+1,n,R_1,R_lm1);        
//        ret = p0 - p1;
//    end
//end
//
//end
template <class T>
inline T VSSphericalHarmonicsRotator<T>::rot_W(int l, int m, int n) {
	if (m == 0) {
		fprintf(stderr, "m cannot be 0\n");
		return (T)0;
	}
	else {
		if (m > 0)
			return rot_P(1, l, m + 1, n) + rot_P(-1, l, -m - 1, n);
		else
		    return rot_P(1, l, m - 1, n) - rot_P(-1, l, -m + 1, n);
	}
}


//function [ret] = V(l,m,n,R_1,R_lm1)
//
//if (m==0)
//    p0 = P(1,l,1,n,R_1,R_lm1);
//    p1 = P(-1,l,-1,n,R_1,R_lm1);
//    ret = p0+p1;
//else
//    if (m>0)
//        d = (m==1);
//        p0 = P(1,l,m-1,n,R_1,R_lm1);
//        p1 = P(-1,l,-m+1,n,R_1,R_lm1);        
//        ret = p0*sqrt(1+d) - p1*(1-d);
//    else
//        d = (m==-1);
//        p0 = P(1,l,m+1,n,R_1,R_lm1);
//        p1 = P(-1,l,-m-1,n,R_1,R_lm1);        
//        ret = p0*(1-d) + p1*sqrt(1+d);
//    end
//end
//
//end
template <class T>
inline T VSSphericalHarmonicsRotator<T>::rot_V(int l, int m, int n) {
	if (m == 0)
		return  rot_P(1,l,1,n) + rot_P(-1,l,-1,n);
	else{
		if (m > 0) {
			if (m == 1) {
				return rot_P(1, l, m - 1, n) * VS_SQRT_2;
			}
			else {
				return rot_P(1, l, m - 1, n) - rot_P(-1, l, -m + 1, n);
			}
			//return  rot_P(1, l, m - 1, n) * sqrt(1.0 + (m == 1)) - rot_P(-1, l, -m + 1, n) * (1.0 - (m == 1));
		}
		else {
			if (m == -1) {
				return rot_P(-1, l, -m - 1, n)  * VS_SQRT_2;
			}
			else {
				return rot_P(1, l, m + 1, n) + rot_P(-1, l, -m - 1, n);
			}
			//return  rot_P(1, l, m + 1, n) * (1.0 - (m == -1)) + rot_P(-1, l, -m - 1, n) * sqrt(1.0 + (m == -1));
		}
	}
}

//function [ret] = U(l,m,n,R_1,R_lm1)
//
//ret = P(0,l,m,n,R_1,R_lm1);
//
//end
template <class T>
inline T VSSphericalHarmonicsRotator<T>::rot_U(int l, int m, int n) {
	return rot_P(0, l, m, n);
}


template class VSSphericalHarmonicsRotator<float>;
template class VSSphericalHarmonicsRotator<double>;


////Rotate ambisonics _inpx coefficients
////prev_field_orient
////curr_field_orient
//if (_i0->_anch == 4) {
//	for (k = 0; k < _bsze; ++k) {
//		SLERP_field_orient = QuaternionSlerp(prev_field_orient, curr_field_orient, ((double)k) / _bsze);
//		Euler_angles = Quaternion2EulerZYXIntrinsic(SLERP_field_orient);
//
//		//Generate rotation matrix from Tait-Bryan ZYX angles
//		//https://en.wikipedia.org/wiki/Euler_angles#Tait.E2.80.93Bryan_angles
//		cx = cos(Euler_angles.x);
//		cy = cos(Euler_angles.y);
//		cz = cos(Euler_angles.z);
//		sx = sin(Euler_angles.x);
//		sy = sin(Euler_angles.y);
//		sz = sin(Euler_angles.z);
//
//		rot_m[0][0] = cx*cy;	rot_m[0][1] = cx*sy*sz - cz*sx;		rot_m[0][2] = sx*sz + cx*cz*sy;
//		rot_m[1][0] = cy*sx;	rot_m[1][1] = cx*cz + sx*sy*sz;		rot_m[1][2] = cz*sx*sy - cx*sz;
//		rot_m[2][0] = -sy;		rot_m[2][1] = cy*sz;				rot_m[2][2] = cy*cz;
//
//		//Apply rotation to input _inpx's x y z components
//		for (p = 0; p < _bsze; p++) {
//			for (l = 0; l < 3; ++l)
//				rot_inpx[l] = rot_m[l][0] * _inpx[1][p] + rot_m[l][1] * _inpx[2][p] + rot_m[l][2] * _inpx[3][p];
//			for (l = 0; l < 3; ++l)
//				_inpx[l + 1][p] = rot_inpx[l];
//		}
//	}
//}
//
//





//Calling getSHRotMtx
//Nchan = size(hoasig, 2);
//Nord = sqrt(Nchan) - 1;
//
//% get rotation matrix
//Rzyx = euler2rotationMatrix(-yaw*pi / 180, -pitch*pi / 180, roll*pi / 180, 'zyx');
//
//% compute rotation matrix in the SH domain
//Rshd = getSHrotMtx(Rzyx, Nord, 'real');
//
//% apply to hoa signals
//hoasig_rot = hoasig * Rshd.';
//


//function R = getSHrotMtx(Rxyz, L, basisType)
//% GETSHROTMTX Rotation matrices of real / complex spherical harmonics
//%   GETSHROTMTX computes the rotation matrices for real spherical
//%   harmonics according to the recursive method of Ivanic and Ruedenberg,
//%   as can be found in
//%
//%       Ivanic, J., Ruedenberg, K. (1996).Rotation Matrices for Real
//%       Spherical Harmonics.Direct Determination by Recursion.The Journal
//%       of Physical Chemistry, 100(15), 6342 ? 6347.
//%
//%   and with the corrections of
//%
//%       Ivanic, J., Ruedenberg, K. (1998).Rotation Matrices for Real
//%       Spherical Harmonics.Direct Determination by Recursion Page : Additions
//%       and Corrections.Journal of Physical Chemistry A, 102(45), 9099 ? 9100.
//%
//%   The code implements directly the equations of the above publication and
//%   is based on the code, with modifications, found in submision
//%   http ://www.mathworks.com/matlabcentral/fileexchange/15377-real-valued-spherical-harmonics
//	%   by Bing Jian
//	%   and the C++ implementation found at
//	%   http ://mathlib.zfx.info/html_eng/SHRotate_8h-source.html.
//	%
//	%   Apart from the real rotation matrices, for which the algorithm is
//	%   defined, the function returns also the complex SH rotation matrices, by
//	%   using the transformation matrices from real to complex spherical
//	%   harmonics.This way bypasses computation of the complex rotation
//	%   matrices based on explicit formulas of Wigner - D matrices, which would
//	%   have been much slower and less numerically robust.
//	%
//	%   TODO: Recursive algorithm directly for complex SH as found in
//	%
//	%       Choi, C.H., Ivanic, J., Gordon, M.S., &Ruedenberg, K. (1999).Rapid
//	%       and stable determination of rotation matrices between spherical
//	%       harmonics by direct recursion.The Journal of Chemical Physics,
//	% 111(19), 8825.
//	%
//	%   Inputs:
//%
//%       L : the maximum band L up to which the rotation matrix is computed
//%       Rxyz : the normal 3x3 rotation matrix for the cartesian system
//%       basisType : 'real' or 'complex' SH
//%
//%   Outputs :
//	%
//	%       R : the(L + 1) ^ 2x(L + 1) ^ 2 block diagonal matrix that rotates the frame
//	%       to the desired orientation
//	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	%
//	%   Archontis Politis, 10 / 06 / 2015
//	% archontis.politis@aalto.fi
//	%
//	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//	if nargin<3
//		basisType = 'real';
//end
//% allocate total rotation matrix
//R = zeros(L + 1) ^ 2;
//
//% initialize zeroth and first band rotation matrices for recursion
//%	Rxyz = [Rxx Rxy Rxz
//%           Ryx Ryy Ryz
//%           Rzx Rzy Rzz]
//%
//% zeroth - band(l = 0) is invariant to rotation
//R(1) = 1;
//
//% the first band(l = 1) is directly related to the rotation matrix
//R_1(-1 + 2, -1 + 2) = Rxyz(2, 2);
//R_1(-1 + 2, 0 + 2) = Rxyz(2, 3);
//R_1(-1 + 2, 1 + 2) = Rxyz(2, 1);
//R_1(0 + 2, -1 + 2) = Rxyz(3, 2);
//R_1(0 + 2, 0 + 2) = Rxyz(3, 3);
//R_1(0 + 2, 1 + 2) = Rxyz(3, 1);
//R_1(1 + 2, -1 + 2) = Rxyz(1, 2);
//R_1(1 + 2, 0 + 2) = Rxyz(1, 3);
//R_1(1 + 2, 1 + 2) = Rxyz(1, 1);
//
//R(2:4, 2 : 4) = R_1;
//R_lm1 = R_1;
//
//% compute rotation matrix of each subsequent band recursively
//band_idx = 4;
//for l = 2:L
//
//R_l = zeros(2 * l + 1);
//for m = -l:l
//for n = -l : l
//
//% compute u, v, w terms of Eq.8.1 (Table I)
//d = (m == 0); % the delta function d_m0
//if abs(n) == l
//denom = (2 * l)*(2 * l - 1);
//else
//denom = (l*l - n*n);
//end
//u = sqrt((l*l - m*m) / denom);
//v = sqrt((1 + d)*(l + abs(m) - 1)*(l + abs(m)) / denom)*(1 - 2 * d)*0.5;
//w = sqrt((l - abs(m) - 1)*(l - abs(m)) / denom)*(1 - d)*(-0.5);
//
//% computes Eq.8.1
//if u~= 0, u = u*U(l, m, n, R_1, R_lm1); end
//if v~= 0, v = v*V(l, m, n, R_1, R_lm1); end
//if w~= 0, w = w*W(l, m, n, R_1, R_lm1); end
//R_l(m + l + 1, n + l + 1) = u + v + w;
//end
//end
//R(band_idx + (1:2 * l + 1), band_idx + (1:2 * l + 1)) = R_l;
//R_lm1 = R_l;
//band_idx = band_idx + 2 * l + 1;
//end
//
//% if the rotation matrix is needed for complex SH, then get it from the one
//% for real SH by the real - to - complex - transformation matrices
//if isequal(basisType, 'complex')
//W = complex2realSHMtx(L);
//R = W.'*R*conj(W);
//end
//
//end
//
//
