#include "vsSphericalHarmonicsReal.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
VSSphericalHarmonicsUtils<T>::VSSphericalHarmonicsUtils(int _max_precomputed_order) {
	init(_max_precomputed_order);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsUtils<T>::init(int _max_precomputed_order) {
	max_precomputed_order = _max_precomputed_order;

	updateFactorialTable(2* max_precomputed_order);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//P_n(x) = 1/(2^n) sum_r (-1)^r (2(n-r))! / (r! (n-r)! (n-2r)!) x^(n-2r)
////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
T VSSphericalHarmonicsUtils<T>::LegendrePolyRodriguesRef(T x, int n) {
	T p = (T)0;

	//Naive version using factorials
	int r = 0;
	while(n - 2*r >= 0) {
		p += (factorialRef(2*(n-r)) / (factorialRef(r) * factorialRef(n-r) * factorialRef(n-2*r))) * pow(x, n-2*r);
		++r;
	}
	p /= pow(2, n);
	return p;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
T VSSphericalHarmonicsUtils<T>::LegendrePolyRodriguesFast(T x, int n) {
	T p = (T)0;

	////Naive version using factorials
	//int r = 0;
	//while (n - 2 * r >= 0) {
	//	p += (factorial(2 * (n - r)) / (factorial(r) * factorial(n - r) * factorial(n - 2 * r))) * pow(x, n - 2 * r);
	//	++r;
	//}
	//p /= pow(2, n);
	return p;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
long long VSSphericalHarmonicsUtils<T>::factorialRef(int n) {
	long long m = 1;
	for (int i = 1; i <= n; ++i)
		m *= i;
	return m;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
inline long long VSSphericalHarmonicsUtils<T>::factorialFast(int n) {
	int f_table_size = factorial_table.size();
	if (n < f_table_size)
		return factorial_table[n];
	else {
		long long m = factorial_table[f_table_size - 1];
		for (int i = f_table_size; i <= n; ++i)
			m *= i;
		return m;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSSphericalHarmonicsUtils<T>::updateFactorialTable(int n) {
	factorial_table.resize(n + 1);
	factorial_table[0] = 1;
	for (int i = 1; i <= n; ++i)
		factorial_table[i] = i * factorial_table[i-1];
}





template class VSSphericalHarmonicsUtils<float>;
template class VSSphericalHarmonicsUtils<double>;

template class VSSphericalHarmonicsReal<float>;
template class VSSphericalHarmonicsReal<double>;