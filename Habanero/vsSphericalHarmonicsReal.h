#ifndef _VS_SPHERICAL_HARMONICS_REAL_H_
#define _VS_SPHERICAL_HARMONICS_REAL_H_

#include <math.h>
#include <complex>

#include "vsMatrix.h"

template <class T>
class VSSphericalHarmonicsReal {
public:

private:
	
};


//Utility class
template <class T>
class VSSphericalHarmonicsUtils {
public:
	VSSphericalHarmonicsUtils(int _max_precomputed_order = 10);
	void init(int _max_precomputed_order);

	long long factorialRef(int n);
	inline long long factorialFast(int n);

	T LegendrePolyRodriguesRef(T x, int n);	//Reference solution
	T LegendrePolyRodriguesFast(T x, int n);	
private:
	int max_precomputed_order;
	EXVector(long long) factorial_table;			//Factorial table

	void updateFactorialTable(int n);				//Updates factorial table upto n

	//Note, maybe worth precomputing and storing some in executable code. Extern it...
};



#endif