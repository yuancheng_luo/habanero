#include "vsQuaternion.h"

/////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T>::VSQuaternion() {
	w = 0;
	x = 0;
	y = 0;
	z = 0;
}

/////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T>::VSQuaternion(T _w, T _x, T _y, T _z) {
	w = _w;
	x = _x;
	y = _y;
	z = _z;
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T> VSQuaternion<T>::Quaternion2EulerZYXIntrinsic() const{
	VSVector3<T> v;

	v.x = atan2(2 * (y * z + w * x), w * w - x * x - y * y + z * z);
	v.y = asin(-2 * (x * z - w * y));
	v.z = atan2(2 * (x * y + w * z), w*w + x*x - y*y - z * z);

	return v;
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T>  VSQuaternion<T>::EulerZYXIntrinsic2Quaternion(const VSVector3<T> & euler_angle){

	T c_x = cos(euler_angle.x / 2.0);
	T s_x = sin(euler_angle.x / 2.0);

	T c_y = cos(euler_angle.y / 2.0);
	T s_y = sin(euler_angle.y / 2.0);

	T c_z = cos(euler_angle.z / 2.0);
	T s_z = sin(euler_angle.z / 2.0);

	VSQuaternion<T> q;

	q.w = c_x * c_y * c_z + s_x * s_y * s_z;
	q.x = s_x * c_y * c_z - c_x * s_y * s_z;
	q.y = c_x * s_y * c_z + s_x * c_y * s_z;
	q.z = c_x * c_y * s_z - s_x * s_y * c_z;

	return q;
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
T VSQuaternion<T>::QuaternionInnerProduct(const VSQuaternion<T>  &   p, const VSQuaternion<T>  &   q) {
	return p.w * q.w + p.x * q.x + p.y * q.y + p.z * q.z;
}

/////////////////////////////////////////////////////////////////////////////
template <class T>
inline T  VSQuaternion<T>::norm() const {
	return sqrt(QuaternionInnerProduct(*this, *this));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T> VSQuaternion<T>::QuaternionConj() const {
	return VSQuaternion<T>(w, -x, -y, -z);
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T>  VSQuaternion<T>::QuaternionNormalized() const{
	VSQuaternion<T>  p;
	T n = norm();
	
	p.w = w / n;
	p.x = x / n;
	p.y = y / n;
	p.z = z / n;

	return p;
}

/////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T>  VSQuaternion<T>::QuaternionSLERP(const VSQuaternion<T>  &   p, const VSQuaternion<T>  &   q, T lambda) {

	VSQuaternion<T> p_normalized = p.QuaternionNormalized();
	VSQuaternion<T> q_normalized = q.QuaternionNormalized();

	T dot_product = QuaternionInnerProduct(p_normalized, q_normalized);
	if (fabs(dot_product) >= 1.0) {//Quaternion ends are same 
		return p;
	}

	if (dot_product < 0) {
		p_normalized.w = -p_normalized.w;
		p_normalized.x = -p_normalized.x;
		p_normalized.y = -p_normalized.y;
		p_normalized.z = -p_normalized.z;
		dot_product = -dot_product;
	}

	T theta = (T)acos(dot_product);

	if (fabs(theta) == acos(0)) {	//Quaternions on opposite sides
		theta -= .0001;				//Default to one side
	}

	T st = (T)sin(theta);

	T wt_high = (T)sin(lambda * theta) / st;
	T wt_low = (T)sin((1.0 - lambda)*theta) / st;

	VSQuaternion<T>  qr;
	qr.w = p_normalized.w * wt_low + q_normalized.w * wt_high;
	qr.x = p_normalized.x * wt_low + q_normalized.x * wt_high;
	qr.y = p_normalized.y * wt_low + q_normalized.y * wt_high;
	qr.z = p_normalized.z * wt_low + q_normalized.z * wt_high;

	return qr.QuaternionNormalized();
}
/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T>	VSQuaternion<T>::QuaternionVectorRotate(const VSQuaternion<T> & q, const VSVector3<T> & v) {
	VSQuaternion<T> p = VSQuaternion(0, v.x, v.y, v.z);
	VSQuaternion<T> q_unit = q.QuaternionNormalized();
	VSQuaternion<T> q_unit_conj = q_unit.QuaternionConj();
	VSQuaternion r =  QuaternionHamiltonProduct(QuaternionHamiltonProduct(q_unit, p), q_unit_conj);

	return VSVector3<T>(r.x, r.y, r.z);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <class T>
VSQuaternion<T> VSQuaternion<T>::QuaternionHamiltonProduct(const VSQuaternion<T> & u, const VSQuaternion<T> & v) {
	return VSQuaternion(	u.w * v.w - u.x * v.x - u.y * v.y - u.z * v.z,
							u.w * v.x + u.x * v.w + u.y * v.z - u.z * v.y,
							u.w * v.y - u.x * v.z + u.y * v.w + u.z * v.x,
							u.w * v.z + u.x * v.y - u.y * v.x + u.z * v.w);
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T>	 VSQuaternion<T>::EulerZYXIntrinsicSLERP(const VSVector3<T> & p, const VSVector3<T> & q, T lambda) {
	VSQuaternion<T> quat_p = EulerZYXIntrinsic2Quaternion(p);
	VSQuaternion<T> quat_q = EulerZYXIntrinsic2Quaternion(q);

	VSQuaternion<T> SLERP_pq = QuaternionSLERP(quat_p, quat_q, lambda);
	return SLERP_pq.Quaternion2EulerZYXIntrinsic();
}
/////////////////////////////////////////////////////////////////////////////
template <class T>
void VSQuaternion<T>::print() const {
	std::cout << "(" << w << "," << x << "," << y<< "," << z  << ")\n";
}

/////////////////////////////////////////////////////////////////////////////
template <class T>
void VSQuaternion<T>::printAngleAxisForm() const {
	T angle = acos(w) * 2;
	T s = sin(angle / 2);
	std::cout << "Angle-Axis (" << angle << "," << x / s << "," << y / s << "," << z / s << ")\n";
}

//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
bool VSQuaternion<T>::equals(const VSQuaternion<T> &   q) const {
	return (w == q.w) &&  (x == q.x) && (y == q.y) && (z == q.z);
}

//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
bool VSQuaternion<T>::equals(const VSQuaternion<T> &  q, T tol)  const {
	return   (abs(w - q.w) <= tol) && (abs(x - q.x) <= tol) && (abs(y - q.y) <= tol) && (abs(z - q.z) <= tol);
}

template class VSQuaternion<float>;
template class VSQuaternion<double>;
