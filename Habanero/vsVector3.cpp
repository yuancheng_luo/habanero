﻿#include "vsVector3.h"

/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T>::VSVector3() {
	x = (T)0;
	y = (T)0;
	z = (T)0;
}
/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T>::VSVector3(T _x, T _y, T _z) {
	x = _x;
	y = _y;
	z = _z;
}


/////////////////////////////////////////////////////////////////////////////
template <class T>
void VSVector3<T>::EulerZYXIntrinsic2Matrix(T R_mtx[3][3]) const {
	T c_x = cos(x);
	T s_x = sin(x);

	T c_y = cos(y);
	T s_y = sin(y);

	T c_z = cos(z);
	T s_z = sin(z);

	R_mtx[0][0] = c_y * c_z;		R_mtx[0][1] = s_y * s_x * c_z - s_z * c_x;		R_mtx[0][2] = s_y * c_x * c_z + s_z * s_x;
	R_mtx[1][0] = c_y * s_z;		R_mtx[1][1] = s_y * s_x * s_z + c_z * c_x;		R_mtx[1][2] = s_y * c_x * s_z - c_z * s_x;
	R_mtx[2][0] = -s_y;				R_mtx[2][1] = c_y * s_x;						R_mtx[2][2] = c_y * c_x;
}
/////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T> VSVector3<T>::Matrix2EulerZYXIntrinsic(T const R_mtx[3][3]){
	VSVector3<T> euler_zyx;
	if(R_mtx[2][0] < 1.0){
		if(R_mtx[2][0] > -1){
			euler_zyx.y  = asin(-R_mtx[2][0]);
			euler_zyx.z = atan2(R_mtx[1][0], R_mtx[0][0]);
			euler_zyx.x = atan2(R_mtx[2][1], R_mtx[2][2]);
		}else // Singularity
		{
			euler_zyx.y = VS_PI / 2;
			euler_zyx.z = -atan2(-R_mtx[1][2], R_mtx[1][1]);
			euler_zyx.x = 0;
		}
	}
	else
	{	//Singularity
		euler_zyx.y = -VS_PI / 2;
		euler_zyx.z = atan2(-R_mtx[1][2], R_mtx[1][1]);
		euler_zyx.x = 0;
	}
	return euler_zyx;
}
//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
VSVector3<T> VSVector3<T>::Matrix3x3Vector3Product(T const R_mtx[3][3], const VSVector3<T> & v){
	return VSVector3<T>(	R_mtx[0][0] * v.x + R_mtx[0][1] * v.y + R_mtx[0][2] * v.z,
						R_mtx[1][0] * v.x + R_mtx[1][1] * v.y + R_mtx[1][2] * v.z, 
						R_mtx[2][0] * v.x + R_mtx[2][1] * v.y + R_mtx[2][2] * v.z);
}

//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
void VSVector3<T>::print() const {
	std::cout << "(" << x << "," << y << "," << z << ")\n";
}

//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
bool VSVector3<T>::equals(const VSVector3<T> &  v) const{
	return (x == v.x) && (y == v.y) && (z == v.z);
}

//////////////////////////////////////////////////////////////////////////////////////////
template <class T>
bool VSVector3<T>::equals(const VSVector3<T> &  v, T tol) const{
	return (abs(x - v.x) <= tol) && (abs(y - v.y) <= tol) && (abs(z - v.z) <= tol);
}

template class VSVector3<float>;
template class VSVector3<double>;
