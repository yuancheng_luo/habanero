#include "vsMatrix.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>::Matrix() {
	rows = 0;
	cols = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>::Matrix(int _rows, int _cols) {
	mat.resize(_rows);
	for (int i = 0; i < _rows; i++) {
		mat[i].resize(_cols);
	}
	rows = _rows;
	cols = _cols;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>::Matrix(int _rows, int _cols, const T& _initial) {
	mat.resize(_rows);
	for (int i = 0; i < _rows; i++) {
		mat[i].resize(_cols, _initial);
	}
	rows = _rows;
	cols = _cols;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>::Matrix(const EXVector(T) & rhs) {
	rows = 1;
	cols = rhs.size();

	mat.resize(rows);
	mat[0].resize(cols);
	for (int i = 0; i < cols; ++i) {
		mat[0][i] = rhs[i];
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>::~Matrix() {}	//vectors will be cleared

						///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::resize(int _rows, int _cols) {
	if (_rows != rows) {
		mat.resize(_rows);
		rows = _rows;
	}
	if (_cols != cols) {
		for (int i = 0; i < _rows; i++) {
			mat[i].resize(_cols);
		}
		cols = _cols;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::resize(int _rows, int _cols, const T & _initial) {
	if (_rows != rows) {
		mat.resize(_rows);
		rows = _rows;
	}
	if (_cols != cols) {
		for (int i = 0; i < _rows; i++) {
			mat[i].resize(_cols, _initial);
		}
		cols = _cols;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
int Matrix<T>::getNumRows() const {
	return rows;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
int Matrix<T>::getNumCols() const {
	return cols;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
const EXVector(T)* Matrix<T>::getRowPtr(int _row) const {
	if (_row < rows) {
		return &mat[_row];
	}
	else {
		return NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
const EXVector(T) &  Matrix<T>::getRow(int _row) const {
	return mat[_row];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::setRow(int _row, const EXVector(T) & _row_data) {
	if (_row_data.size() == cols && _row < rows) {
		mat[_row] = _row_data;
		return true;
	}
	else {
		return false;
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::getCol(int _col, EXVector(T) & col_vector) const {
	if (_col < cols) {
		if (col_vector.size() != rows)
			col_vector.resize(rows);
		for (int i = 0; i < rows; ++i)
			col_vector[i] = mat[i][_col];
		return true;
	}
	else
		return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::setCol(int _col, const  EXVector(T) & col_vector) {
	if (col_vector.size() == rows && _col < cols) {
		for (int i = 0; i < rows; ++i) {
			mat[i][_col] = col_vector[i];
		}
		return true;
	}
	else
		return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::freeMatrix() {
	for (int i = 0; i < rows; ++i) {
		mat[i].clear();
	}
	mat.clear();
	rows = 0;
	cols = 0;
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
T & Matrix<T>::operator()(const int & row, const int & col) {
	return this->mat[row][col];
}

// Access the individual elements (const)                                                                                                                                     
template<class T>
const T & Matrix<T>::operator()(const int & row, const int & col) const {
	return this->mat[row][col];
}

// Access the individual elements (const)                                                                                                                                     
template<class T>
const T * Matrix<T>::mat_ptr(const int & row, const int & col) {
	return &this->mat[row][col];
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
T & Matrix<T>::MAT_ij(const int & row, const int & col) {
	return this->mat[row - 1][col - 1];
}

// Access the individual elements (const)                                                                                                                                     
template<class T>
const T & Matrix<T>::MAT_ij(const int & row, const int & col) const {
	return this->mat[row - 1][col - 1];
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& rhs) {
	if (&rhs == this)
		return *this;

	int new_rows = rhs.getNumRows();
	int new_cols = rhs.getNumCols();

	if (rows != new_rows)
		mat.resize(new_rows);

	for (int i = 0; i<new_rows; i++) {
		if (cols != new_cols)
			mat[i].resize(new_cols);
	}

	for (int i = 0; i < new_rows; i++) {
		for (int j = 0; j < new_cols; j++) {
			mat[i][j] = rhs(i, j);
		}
	}
	rows = new_rows;
	cols = new_cols;

	return *this;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator+(const T& rhs)  const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] + rhs;
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator-(const T& rhs) const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] - rhs;
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator*(const T& rhs) const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] * rhs;
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator/(const T& rhs) const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] / rhs;
		}
	}

	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator+(const Matrix<T>& rhs) const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] + rhs(i, j);
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& rhs)  const {
	Matrix result(rows, cols);
	for (int i = 0; i<rows; i++) {
		for (int j = 0; j<cols; j++) {
			result(i, j) = this->mat[i][j] - rhs(i, j);
		}
	}

	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator*(const Matrix<T>& rhs) const {
	int _rows = rows;
	int _cols = rhs.getNumCols();
	Matrix result(_rows, _cols, (T)0);

	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			for (int k = 0; k < cols; k++) {
				result(i, j) += this->mat[i][k] * rhs(k, j);
			}
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::operator^(int p) const {

	if (p == 0)
		return Matrix<T>::eye(rows, cols);
	else if (p == 1)
		return *this;
	else {
		Matrix<T> half = (*this) ^ (p / 2);
		if (p % 2 == 0)
			return half * half;
		else
			return half * half * (*this);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::operator==(const Matrix<T> & rhs) const {
	if (rows != rhs.getNumRows() || cols != rhs.getNumCols())
		return false;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (mat[i][j] != rhs(i, j))
				return false;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::equals(const Matrix<T> & rhs) const {
	return *this == rhs;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::equals(const Matrix<T> & rhs, const T & tolerance) const {
	if (rows != rhs.getNumRows() || cols != rhs.getNumCols())
		return false;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (abs(mat[i][j] - rhs(i, j)) > tolerance)
				return false;
		}
	}

	return true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
EXVector(T) Matrix<T>::operator*(const EXVector(T)& rhs) const {
	EXVector(T) out;
	out.resize(rows);
	int rhs_rows = rhs.size();
	if (rhs_rows == cols) {
		for (int i = 0; i < rows; ++i) {
			out[i] = 0;
			for (int j = 0; j < cols; ++j) {
				out[i] += mat[i][j] * rhs[j];
			}
		}
	}
	return out;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::MVP(const EXVector(T) & in, EXVector(T) & out) const {
	int in_rows = in.size();
	if (in_rows == cols) {
		for (int i = 0; i < rows; ++i) {
			out[i] = 0;
			for (int j = 0; j < cols; ++j) {
				out[i] += mat[i][j] * in[j];
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Matrix<float>::MVP_SSE2(const EXVector(float) & in, EXVector(float) & out) const {
	int in_rows = in.size();
	if (in_rows == cols) {
		__m128 SSE_A, SSE_B, SSE_C;
		int cols_mod_4 = cols & 0x3;

		for (int i = 0; i < rows; ++i) {
			switch (cols_mod_4) {
			case 3:
				SSE_C = _mm_set_ps(0.0f, mat[i][2] * in[2], mat[i][1] * in[1], mat[i][0] * in[0]);	break;
			case 2:
				SSE_C = _mm_set_ps(0.0f, 0.0f, mat[i][1] * in[1], mat[i][0] * in[0]);	break;
			case 1:
				SSE_C = _mm_set_ps(0.0f, 0.0f, 0.0f, mat[i][0] * in[0]);	break;
			default:
				SSE_C = _mm_set_ps(0.0f, 0.0f, 0.0f, 0.0f);
			}
			for (int j = cols_mod_4; j < cols; j += 4) {
				SSE_A = _mm_loadu_ps(&mat[i][j]);
				SSE_B = _mm_loadu_ps(&in[j]);
				SSE_C = _mm_add_ps(SSE_C, _mm_mul_ps(SSE_A, SSE_B));
			}

			////Horizontal add
			SSE_A = _mm_add_ps(SSE_C, _mm_shuffle_ps(SSE_C, SSE_C, _MM_SHUFFLE(0, 1, 3, 2)));
			SSE_B = _mm_add_ps(SSE_A, _mm_shuffle_ps(SSE_A, SSE_A, _MM_SHUFFLE(0, 1, 0, 1)));
			_mm_store_ss(&out[i], SSE_B);

			/*float v[4];
			_mm_storeu_ps(v, SSE_C);
			out[i] = v[0] + v[1] + v[2] + v[3];*/
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Matrix<double>::MVP_SSE2(const EXVector(double) & in, EXVector(double) & out) const {
	int in_rows = in.size();
	if (in_rows == cols) {
		__m128d SSE_A, SSE_B, SSE_C;
		int cols_mod_2 = cols & 0x1;

		for (int i = 0; i < rows; ++i) {
			switch (cols_mod_2) {
			case 1:
				SSE_C = _mm_set_pd(0.0, mat[i][0] * in[0]);	break;
			default:
				SSE_C = _mm_set_pd(0.0, 0.0);
			}
			for (int j = cols_mod_2; j < cols; j += 2) {
				SSE_A = _mm_loadu_pd(&mat[i][j]);
				SSE_B = _mm_loadu_pd(&in[j]);
				SSE_C = _mm_add_pd(SSE_C, _mm_mul_pd(SSE_A, SSE_B));
			}
			//Horizontal add
			SSE_A = _mm_add_pd(SSE_C, _mm_shuffle_pd(SSE_C, SSE_C, _MM_SHUFFLE2(0, 1)));
			_mm_store_sd(&out[i], SSE_A);


			//double v[2];
			//_mm_storeu_pd(v, SSE_C);
			//out[i] = v[0] + v[1];
		}
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::eye(int _rows, int _cols) {
	Matrix result(_rows, _cols, (T)0);
	int minDimension = (int)fmin(_rows, _cols);

	for (int i = 0; i < minDimension; ++i) {
		result(i, i) = (T)1;
	}
	return result;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
Matrix<T> Matrix<T>::transpose() const {
	Matrix result(cols, rows);

	for (int i = 0; i<cols; i++) {
		for (int j = 0; j<rows; j++) {
			result(i, j) = this->mat[j][i];
		}
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::copyToArray(T * const out) {											//Copy contents of mat to linear array out
	for (int i = 0; i < rows; ++i) {
		memcpy(&out[i*cols], &mat[i][0], sizeof(T) * cols);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::copyToVector(EXVector(T) & out) {								//Copy contents of mat to vector
	out.resize(rows * cols);
	for (int i = 0; i < rows; ++i) {
		memcpy(&out[i*cols], &mat[i][0], sizeof(T) * cols);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
bool Matrix<T>::push_back_row(const EXVector(T) & in) {							//Add non-empty row to end
	int in_cols = in.size();
	if (in_cols > 0) {
		if (cols == 0 || cols == in_cols) {	//cols == 0 implies cold start, cols == in_cols implies dimensions alligned
			mat.push_back(in);

			cols = in_cols;
			rows++;
			return true;
		}
	}

	return false;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<class T>
void Matrix<T>::printToConsole() const {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			std::cout << mat[i][j] << " ";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}

template class Matrix<float>;
template class Matrix<double>;
