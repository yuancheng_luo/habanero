//Habanero v1.0 (9-30-2016)

#ifndef __VS_VECTOR3__
#define __VS_VECTOR3__

#include <iostream>
#include "math.h"

#define VS_SQRT_2	1.4142135623730950488016887242097
#define VS_PI		3.1415926535897932384626433832795

/////////////////////////////////////////////////////////////////////////////////
template <class T>
class VSVector3{
public:
	T x;
	T y;
	T z;

	//Constructors
	VSVector3();
	VSVector3(T _x, T _y, T _z);

	//Tait-Bryan Z-Y'-X'' to Rotation Matrix where x is first row/col (roll), y is second row/col (pitch), z is third row/col (yaw)
	//R_mtx = R_yaw * R_pitch * R_roll, used for premultiplying a column vector
	void EulerZYXIntrinsic2Matrix(T R_mtx[3][3]) const;
	static VSVector3 Matrix2EulerZYXIntrinsic(T const R_mtx[3][3]);

	//Unrolled matrix-vector product
	static VSVector3 Matrix3x3Vector3Product(T const R_mtx[3][3], const VSVector3<T> & v);

	bool equals(const VSVector3<T> &  v) const;
	bool equals(const VSVector3<T> &  v, T tol) const;

	void print() const;
};


#endif