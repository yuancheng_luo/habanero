#include  <iostream>
#include <ctime>
#include "vsSphericalHarmonicsRotator.h"
#include "vsMatrix.h"
#include "vsQuaternion.h"

#define SPH_PRECISION float

template <class T>
void printEXVector(const EXVector(T) & v) {
	int n = v.size();
	for (int i = 0; i < n; ++i)
		std::cout << v[i] << " ";
	std::cout << "\n";
}


void main() {
	int amb_order = 7;
	int num_channels = (amb_order + 1) * (amb_order + 1);
	
	///////////////////////////////////////////////////////////////////////////
	//Test 0.0: Euler to quaternion to Euler equivalences
	///////////////////////////////////////////////////////////////////////////
	VSVector3<SPH_PRECISION> euler_zyx;
	euler_zyx.x = 1;
	euler_zyx.y = VS_PI / 4;
	euler_zyx.z = 2;

	VSQuaternion<SPH_PRECISION> quaternion_test = VSQuaternion<SPH_PRECISION>::EulerZYXIntrinsic2Quaternion(euler_zyx);
	VSVector3<SPH_PRECISION> euler_zyx_test = quaternion_test.Quaternion2EulerZYXIntrinsic();
	

	euler_zyx.print();
	euler_zyx_test.print();
	quaternion_test.print();

	std::cout << "Test 0.0: " << euler_zyx.equals(euler_zyx_test, 1e-6) << "\n\n";


	///////////////////////////////////////////////////////////////////////////
	//Test 0.1: Quaternion and euler Rotation tests
	///////////////////////////////////////////////////////////////////////////
	VSSphericalHarmonicsRotator<SPH_PRECISION> sph_rot = VSSphericalHarmonicsRotator<SPH_PRECISION>(amb_order);

	euler_zyx.x = .5;
	euler_zyx.y = .25;
//	euler_zyx.z = VS_PI * 2; //Full circle Z
//	euler_zyx.z = VS_PI * 1; //half circle Z
	euler_zyx.z = VS_PI * 0.5; //Quarter circle Z

	SPH_PRECISION R_mtx[3][3];
	euler_zyx.EulerZYXIntrinsic2Matrix(R_mtx);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			std::cout << R_mtx[i][j] << " ";
		}
		std::cout << "\n";
	}
	euler_zyx_test = VSVector3<SPH_PRECISION>::Matrix2EulerZYXIntrinsic(R_mtx);
	std::cout << "Test 0.1: " << euler_zyx_test.equals(euler_zyx, 1e-6) << "\n\n";


	///////////////////////////////////////////////////////////////////////////
	//Test 0.2: Rotation tests with data
	///////////////////////////////////////////////////////////////////////////
	VSVector3<SPH_PRECISION> vec_common = VSVector3<SPH_PRECISION>(1, 2, 3);
	VSVector3<SPH_PRECISION> mat_vec_test = VSVector3<SPH_PRECISION>::Matrix3x3Vector3Product(R_mtx, vec_common);
	VSVector3<SPH_PRECISION> quat_vec_test = VSQuaternion<SPH_PRECISION>::QuaternionVectorRotate(VSQuaternion<SPH_PRECISION>::EulerZYXIntrinsic2Quaternion(euler_zyx), vec_common);
	mat_vec_test.print();
	quat_vec_test.print();
	std::cout << "Test 0.2: " << mat_vec_test.equals(quat_vec_test, 1e-6) << "\n\n";

	EXVector(SPH_PRECISION) in_out_stream;
	in_out_stream.resize(num_channels);
	for (int i = 0; i < num_channels; ++i)
		in_out_stream[i] = i;

	//Start clock
	std::clock_t start;
	double duration;
	start = std::clock();
	printEXVector(in_out_stream);
	for (int i = 0; i < 441000; ++i) {	//For speed test, 10 sec worth samples (44100*10)
//	for (int i = 0; i < 10; ++i) {	//For accuracy check

		//euler_zyx.x = (rand()%1000) / 1000.0f * 2 * _PI;
		//euler_zyx.y = (rand() % 1000) / 1000.0f * 2 * _PI;
		//euler_zyx.z = (rand() % 1000) / 1000.0f * 2 *  _PI;

		//////Version 1 dominated by MVP
		//if(i == 0)
		//	sph_rot.rotate_coefficients_inplace(in_out_stream, euler_zyx);
		//else
		//	sph_rot.rotate_coefficients_inplace(in_out_stream);

		//Version 2 dominated by matrix construction
		sph_rot.rotate_coefficients_inplace(in_out_stream, euler_zyx);
	}
	printEXVector(in_out_stream);

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "Duration: " << duration << '\n';



	///////////////////////////////////////////////////////////////////////////
	//Test 0.2: SLERP tests
	///////////////////////////////////////////////////////////////////////////
	VSVector3<SPH_PRECISION> euler_zyx_start(0, 0, 0);
	VSVector3<SPH_PRECISION> euler_zyx_end(0, VS_PI / 4, VS_PI / 2);

	for (double lambda = 0; lambda <= 1; lambda += .1) {
		VSVector3<SPH_PRECISION> euler_zyx_SLERP_lambda = VSQuaternion<SPH_PRECISION>::EulerZYXIntrinsicSLERP(euler_zyx_start, euler_zyx_end, lambda);
		euler_zyx_SLERP_lambda.print();
	}

	getchar();
}